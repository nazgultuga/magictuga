<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/magic/dir-vars.php");
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS')."TobjAutoTemplate.class.php");
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'Session.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DBTOGGLE').'json-cardslist.php');

	$template = new TobjAutoTemplate();
?>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/fb-buttons.css">
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<script src="js/jquery-2.1.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		
		<script src="js/jxMain.js"></script>
		<script src="js/jxForms.js"></script>
		<script src="js/main.js"></script>
		
		<title>Familia Mota & Oliveira</title>
	</head>
	<body>
		<nav class="navbar navbar-default" role="navigation">
			<div class="container-fluid">
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

				</div>

				<div class="navbar-header">
					<a class="navbar-brand" href="#">Magic The Gathering</a>
				</div>

				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li class="active">
							<form class="navbar-form navbar-left" role="search" name="searchCard">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Nome da Carta (Inglês ou Português)" style="width:300px;" onkeyup="searchCards(this);">
								</div>
								<input type="button" class="btn btn-default" value="Pesquisar">
							</form>
						</li>
					</ul>

					<ul class="nav navbar-nav navbar-right">
						<li class="menu-shopcart">
							<a href="http://localhost/magic/?op=shopping">
								<img src="imgs/shopcart.png">
								<?php
									Session::start();
									$carts = Session::get('listcart');
									$ilen  = count($carts);
									$num = 0;
									$total = 0.0;

									for ($i=0; $i<$ilen; ++$i)
									{ 
										$nosso_preco = getPriceDesconto_MagicTuga($carts[$i]['price']);
									 	$num   += $carts[$i]['qtd'];
									 	$total += $carts[$i]['qtd']*$nosso_preco;
									}
									$total = number_format($total,2);
									echo '<span id="menu-shopcart-cards">'.$num.'</span> cartas (<span id="menu-shopcart-total">'.$total.'</span>€)';
								?>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<?php
			$filename = SETPATH('ROOT','PATH_APP_TEMPLATES').'collections_index.html';
			$html = $template->tpl_page($filename,true);
			$template->cleanVariables();
			echo $html;
		?>
		<div class="main-body">
<!--
<pre>
Falta: Paginação, Carrinho-Compras, 
</pre>
-->
			<?php
				$collection = (isset($_GET['collection'])) ? $_GET['collection'] : '';
				$template->setVar('{url_magic_set}',$collection);
				$rare = (isset($_GET['rare'])) ? ('&rare='.$_GET['rare']) : '';
				$template->setVar('{url_rarity}',$rare);
				$type = (isset($_GET['type'])) ? ('&type='.$_GET['type']) : '';
				$template->setVar('{url_type}',$type);

				if (isset($_GET['op']))
				{
					switch ($_GET['op']) {
						case 'inserir':
							{
								$filename = SETPATH('ROOT','PATH_APP_TEMPLATES').'card_information.html';
							}
							break;
						case 'lista':
							{
								$filename = SETPATH('ROOT','PATH_APP_TEMPLATES').'pesquisa.html';
							}
							break;
						case 'shopping':
							{
								$filename = SETPATH('ROOT','PATH_APP_TEMPLATES').'shoppingcart.html';
							}
							break;
						case 'updateprices':
							{
								require_once(SETPATH('ROOT','PATH_APP_CORE_DBTOGGLE').'update_price.php');
								$filename = SETPATH('ROOT','PATH_APP_TEMPLATES').'updateprices.html';
							}
							break;
						default:
							{
								$filename = SETPATH('ROOT','PATH_APP_TEMPLATES').'inicio.html';
							} break;
					}
				}
				else
				{
					#require_once(SETPATH('ROOT','PATH_APP_CORE_DBTOGGLE').'json-cardslist.php');
					#$filename = SETPATH('ROOT','PATH_APP_TEMPLATES').'pesquisa.html';
					$filename = SETPATH('ROOT','PATH_APP_TEMPLATES').'inicio.html';
				}
				
				$html = $template->tpl_page($filename,true);
				unset($template);
				echo $html;
			?>
		</div>
	</body>
</html>