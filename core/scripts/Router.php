<?php

	class Router
	{
		private static $routes = array();
		
		public static function cleanAll()
		{
			self::$routes = array();
		}
		public static function set($AsRoute,$AfCallback=null)
		{
			self::$routes[$AsRoute] = $AfCallback;
		}
		public static function process($Params)
		{
			foreach(self::$routes as $key => $values)
			{
				if ( ((($key !== '') && stristr($Params,$key)) ||
					 (($key === '') && ($Params == null))) && (isset($values)) )
				{
					if (is_string($values))
					{
						if (file_exists($values))
						{
						#	ob_start();
							include($values);
						#	$Result = ob_get_contents();
						#	ob_end_clean();
						#	echo $Result;
						}
					}
					else
					if (is_callable($values))
					{
						call_user_func($values);
					}
					break;
				}
			}
		}
		
		# Redirect browser
		public static function redirect($AsURL,$AbHasServer=false)
		{
			$sServer = ($AbHasServer) ? $_SERVER['SERVER_NAME'] : '';
			header('Location: '.$sServer.$AsURL);
		#	exit();
		}
	}
	

	/**
	 * The application class.
	 *
	 * Handles the request for each call to the application
	 * and calls the chosen controller and method after splitting the URL.
	 *
	 */
	class RouterController
	{
		protected $controller = 'home';
		protected $method = 'index';
		protected $params = array();
		protected $path = '../core/controllers/';
	
		public function __construct()
		{
			$url = $this->parseUrl();
			
			// Does the requested controller exist?
			// If so, set it and unset from URL array
			if(file_exists($this->path.$url[0].'.php'))
			{
				$this->controller = $url[0];
				unset($url[0]);
			}
			
			require_once($this->path.$this->controller.'.php');
			
			#if (class_exists($this->controller,true))
			#$this->controller = new $this->controller();
			
			// Has a second parameter been passed
			// If so, it might be the requested method
			if(isset($url[1]))
			{
				if(method_exists($this->controller, $url[1]))
				{
					$this->method = $url[1];
					unset($url[1]);
				}
			}
	
			// Set parameters to either the array values or an empty array
			$this->params = $url ? array_values($url) : array();
	
			// Call the chosen method on the chosen controller, passing
			// in the parameters array (or empty array if above was false)
			call_user_func_array(array($this->controller, $this->method), $this->params);
		}
	
		public static function parseUrl()
		{
			if(isset($_GET['op']))
			{
				// Explode a trimmed and sanitized URL by /
				if ($_GET['op'][0] === '/')
					$_GET['op'] = substr($_GET['op'],1);
					$_GET['op'] = str_ireplace('-','_',$_GET['op']);
				return $url = explode('/', filter_var(str_ireplace('-','_',rtrim($_GET['op'], '/')), FILTER_SANITIZE_URL));
			}
		}
	}
	
?>