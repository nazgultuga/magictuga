<?php

	class Session
	{
		private static $_sessionStarted = false;
		
		public static function start()
		{
			if (!isset($_SESSION))
			{
				session_start();
				self::$_sessionStarted = true;
			}
			else
			if ((self::$_sessionStarted == false) && ($_SESSION !== 'NULL'))
			{
				session_start();
				self::$_sessionStarted = true;
			}
		}
		public static function destroy()
		{
			self::start();
			session_unset(); # remove all variables in session
			session_destroy();
			self::$_sessionStarted = false;
		}
		
		public static function set($key,$value)
		{
			return $_SESSION[$key] = $value;
		}
		public static function get($key,$notValue=false)
		{
			return (isset($_SESSION[$key])) ? $_SESSION[$key] : $notValue;
		}
	}

?>