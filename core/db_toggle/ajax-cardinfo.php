<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/magic/dir-vars.php");
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS')."TobjAutoTemplate.class.php");
	require_once(SETPATH('ROOT','PATH_APP_CORE_DBTOGGLE').'json-cardslist.php');

	$Result = '';
	if (isset($_GET['multiverseid']) && ($_GET['multiverseid']!==''))
	{
		$Result = searchRows(array('cards.multiverseid'=>$_GET['multiverseid']),
							array(),'cards.name_eng','ASC',0,1);
		$Result = parseCardsInformation($Result);
		

		/* ----------------------------- */
		$template = new TobjAutoTemplate();
		foreach ($Result[0] as $key => &$value)
		{
			$template->setVar('{'.$key.'}',$value);
		}
		$filename = SETPATH('ROOT','PATH_APP_TEMPLATES').'cardinfo.html';
		$Result = $template->tpl_page($filename,true);
		unset($template);
		/* ----------------------------- */
	}
	echo $Result;
?>