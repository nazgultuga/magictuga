<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/magic/dir-vars.php");
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'Session.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_DBTOGGLE').'json-cardslist.php');

	Session::start();
	$cart = Session::get('listcart');
	if (!$cart)
	{
		$cart = array();
	}

	$Result = false;

	if ( (isset($_POST['ajax'])) &&
		 (isset($_POST['multiverseid'])) &&
		 (isset($_POST['action'])) ) 
	{
		$data 	= json_decode($_POST['data'],true);
		$ilen 	= count($cart);
		$found 	= false;
		$tcartas= 0;
		$tpagar = 0.0;

		switch ($_POST['action'])
		{
			case 'toggle':
				{
					for ($i=0; $i<$ilen; ++$i)
					{ 
						if ($cart[$i]['multiverseid'] === $_POST['multiverseid'])
						{
							$cart[$i] = $data;
							$found = true;
							break;
						}
					}
					if (!$found)
					{
						$cart[] = $data;
						$ilen += 1;
					}
					$Result = true;
				} break;
			case 'remove':
				{
					$ilen = count($cart);
					for ($i=0; $i<$ilen; ++$i)
					{ 
						if ($cart[$i]['multiverseid'] === $_POST['multiverseid'])
						{
							unset($cart[$i]);
							$cart = array_values($cart);
							$Result = true;
							$ilen -= 1;
							break;
						}
					}
				} break;
			default:
				{

				} break;
		}
		Session::set('listcart',$cart);
		for ($i=0; $i<$ilen; $i++)
		{ 
			$nosso_preco = getPriceDesconto_MagicTuga($cart[$i]['price']);
			$tcartas += $cart[$i]['qtd'];
			$tpagar  += $cart[$i]['qtd']*$nosso_preco;
		}
		#Session::destroy();
	}

	echo json_encode(array('result'=>$Result,
							'total_cartas'=>$tcartas,
							'total_pagar'=>number_format($tpagar,2),
						)
					);
?>