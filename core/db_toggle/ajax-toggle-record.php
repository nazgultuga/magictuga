<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/magic/dir-vars.php");
	
	$Result = false;
	$NEW_ID = '';

	if (isset($_POST['form']) && isset($_POST['action']))
	{
		$jsonData = json_decode($_POST['data'],true);
		#var_dump($_POST);

		switch($_POST['form'])
		{
			case 'card_information':
				{
					require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblCardInformation.class.php');
					$obj = new dbTblCardInformation();
					
					switch ($_POST['action'])
					{
						case 'append':
							{
								$Result = $obj->INSERT($jsonData);
								$NEW_ID = $Result['NEW_ID'];
								$Result = $Result['Result'];
							}
							break;
						case 'update':
							{
								$Result = $obj->UPDATE($jsonData);
							}
							break;
					}
					unset($obj);
				} break;
		}
	}

	echo json_encode(array('result'=>$Result, 'new_id'=>$NEW_ID));

?>