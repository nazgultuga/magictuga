<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/magic/dir-vars.php");
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblControl.class.php');
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'Session.php');

	function collectionFilter()
	{
		if (isset($_GET['collection']) && ($_GET['collection']!==''))
		{
			$Result = array();
			$Result['url_magic_set']= (isset($_GET['collection'])) ? ('&collection='.$_GET['collection']) : '';
			$Result['url_rarity'] 	= (isset($_GET['rare'])) ? ('&rare='.$_GET['rare']) : '';
			$Result['url_type'] 	= (isset($_GET['type'])) ? ('&type='.$_GET['type']) : '';

			return array('0'=>$Result);
		}
		return array();
	}
	function tableHeadSort()
	{
		$order = (isset($_GET['order'])) ? $_GET['order'] : 'ASC';
		$order = ($order === 'ASC') ? 'DESC' : 'ASC';

		$Result = array('Order_name'	=>'ASC',
						'Order_colors'	=>'ASC',
						'Order_number'	=>'ASC',
						'Order_type'	=>'ASC',
						'Order_manacost'=>'ASC',
						'Order_power'	=>'ASC',
						'Order_rarity'	=>'ASC',
						'Order_stock'	=>'ASC',
						'Order_price'	=>'ASC');

		if (isset($_GET['field']))
		{
			switch ($_GET['field'])
			{
				case 'name':	$Result['Order_name'] 		= $order; break;
				case 'colors':	$Result['Order_colors'] 	= $order; break;
				case 'number':	$Result['Order_number'] 	= $order; break;
				case 'type':	$Result['Order_type'] 		= $order; break;
				case 'manacost':$Result['Order_manacost'] 	= $order; break;
				case 'power':	$Result['Order_power'] 		= $order; break;
				case 'rarity':	$Result['Order_rarity'] 	= $order; break;
				case 'stock':	$Result['Order_stock'] 		= $order; break;
				case 'price':	$Result['Order_price'] 		= $order; break;
			}
		}
		$Result['url_magic_set'] = (isset($_GET['collection'])) ? ('&collection='.$_GET['collection']) : '';
		return array('0'=>$Result);
	}

	function searchRows($arrWhereAND,$arrWhereOR, $AsFieldToOrder,$OrderBy='ASC', $iPage=0,$iLimPerPage=50)
	{
		$dbTblControl = new dbTblControl();

		$sLIMIT 	 = $dbTblControl->processLIMIT($iPage,$iLimPerPage);
		$arrWhereAND = (count($arrWhereAND)==0) ? 0 : array('AND' => $arrWhereAND);
		$arrWhereOR  = (count($arrWhereOR)==0) ? 0 : array('OR' => $arrWhereOR);
		
		if ((is_array($arrWhereAND['AND'])) && (!is_array($arrWhereOR['OR'])))
		{
			$arraySearch = $arrWhereAND;
		}
		else
		if ((!is_array($arrWhereAND['AND'])) && (is_array($arrWhereOR['OR'])))
		{
			$arraySearch = $arrWhereOR;
		}
		else
		if ((is_array($arrWhereAND['AND'])) && (is_array($arrWhereOR['OR'])))
		{
			$arraySearch = array('AND'=>$arrWhereAND,'OR'=>$arrWhereOR);
		}
		else
		{
			$arraySearch = '';
		}

		if ($AsFieldToOrder === '')
			$AsFieldToOrder = 'cards.magic_set';

		#$dbTblControl = new dbTblControl();
		$Result = $dbTblControl->findInTable('cards',
									array("cards.id AS 'idRow'",
										  "cards.id_collection AS 'id_collection'",
										  
										  "cards.magic_set AS 'magic_set'",
										  "cards.type AS 'type'",
										  "cards.multiverseid AS 'multiverseid'",
										  "cards.name_eng AS 'name_eng'",
										  "cards.name_pt AS 'name_pt'",
										  "cards.colors AS 'colors'",
										  "cards.manaCost AS 'manaCost'",
										  "cards.rarity AS 'rarity'",
										  "cards.power AS 'power'",
										  "cards.toughness AS 'toughness'",

										  "cards.stock AS 'stock'",
										  "cards.price AS 'price'",

										  "cards.text_eng AS 'text_eng'",
										  "cards.text_pt AS 'text_pt'",
										  "cards.number AS 'number'",

										  "collections.name_eng AS 'collection_eng'",
										  "collections.name_pt AS 'collection_pt'",
										  #"collections.magic_set AS 'magic_set'",
										),
									$arraySearch,
									'LEFT JOIN collections ON collections.id=cards.id_collection',
									array('field'=>$AsFieldToOrder, 'order'=>$OrderBy),
									$sLIMIT,
									array('ENCRYPT','ENCRYPT',
										  'STRING','STRING','STRING','STRING','STRING','STRING','STRING','STRING','STRING','STRING',
										  'INTEGER','FLOAT',
										  'STRING','STRING','INTEGER',
										  'STRING','STRING'),
									'',
									true);
		unset($dbTblControl);

		return $Result;
	}

	function buildSelectStock($iNumber,$index=false)
	{
		if ($iNumber>0)
		{
			$Result = array();

			$Result[] = (!$index) ? '<select name="quantidade" width="2">' : 
									'<select name="quantidade" width="2" onchange="updateQuantity(this);">';

			for($i=0; $i<$iNumber; ++$i)
			{
				$Result[] = '<option value="'.($i+1).'" '.(($index == ($i+1))?'selected':'').'>'.($i+1).'</option>';
			}
			$Result[] = '</select>';
			if (!$index)
				$Result[] = '<input type="button" value="Adicionar" class="uibutton" onclick="addCard(this);">';

			return implode("\n",$Result);
		}
		else
		{
			return 'Indisponível';
		}
	}
	function replaceMana($text)
	{
		$text = str_ireplace('{W}','<img class="img-mana" src="http://mtgimage.com/symbol/mana/w.svg">', $text);
		$text = str_ireplace('{U}','<img class="img-mana" src="http://mtgimage.com/symbol/mana/u.svg">', $text);
		$text = str_ireplace('{B}','<img class="img-mana" src="http://mtgimage.com/symbol/mana/b.svg">', $text);
		$text = str_ireplace('{R}','<img class="img-mana" src="http://mtgimage.com/symbol/mana/r.svg">', $text);
		$text = str_ireplace('{G}','<img class="img-mana" src="http://mtgimage.com/symbol/mana/g.svg">', $text);
		$text = str_ireplace('{1}','<img class="img-mana" src="http://mtgimage.com/symbol/mana/1.svg">', $text);
		$text = str_ireplace('{2}','<img class="img-mana" src="http://mtgimage.com/symbol/mana/2.svg">', $text);
		$text = str_ireplace('{3}','<img class="img-mana" src="http://mtgimage.com/symbol/mana/3.svg">', $text);
		$text = str_ireplace('{4}','<img class="img-mana" src="http://mtgimage.com/symbol/mana/4.svg">', $text);
		$text = str_ireplace('{5}','<img class="img-mana" src="http://mtgimage.com/symbol/mana/5.svg">', $text);
		$text = str_ireplace('{6}','<img class="img-mana" src="http://mtgimage.com/symbol/mana/6.svg">', $text);
		$text = str_ireplace('{7}','<img class="img-mana" src="http://mtgimage.com/symbol/mana/7.svg">', $text);
		$text = str_ireplace('{8}','<img class="img-mana" src="http://mtgimage.com/symbol/mana/8.svg">', $text);
		$text = str_ireplace('{9}','<img class="img-mana" src="http://mtgimage.com/symbol/mana/9.svg">', $text);
		$text = str_ireplace('{10}','<img class="img-mana" src="http://mtgimage.com/symbol/mana/10.svg">', $text);
		$text = str_ireplace('{11}','<img class="img-mana" src="http://mtgimage.com/symbol/mana/11.svg">', $text);
		$text = str_ireplace('{12}','<img class="img-mana" src="http://mtgimage.com/symbol/mana/12.svg">', $text);
		$text = str_ireplace('{X}','<img class="img-mana" src="http://mtgimage.com/symbol/mana/x.svg">', $text);
		$text = str_ireplace('{T}','<img class="img-mana" src="http://mtgimage.com/symbol/other/t/24.png">', $text);
		$text = str_ireplace('{Q}','<img class="img-mana" src="http://mtgimage.com/symbol/other/q/24.png">', $text);
		return $text;
	}

	function cardslist()
	{
		$search = isset($_GET['search']) ? $_GET['search'] : '';
		if ($search !== '')
		{
			$arrFields = array(#'cards.type',
							   'cards.multiverseid',
							   'cards.name_eng','cards.name_pt',
							   #'cards.text_eng','cards.text_pt',
							   #'cards.colors',
							   #'cards.number'
							   );
			$arrValue = array($search,$search,$search);
			$search = true;
		}
		else
		{
			$arrFields = array();
			$arrValue = array();
			$search = false;
		}

		$page = isset($_GET['page']) ? $_GET['page'] : '';
		$page = ($page !== '') ? $page : 0;

		$field = isset($_GET['field']) ? $_GET['field'] : '';
		if ($field === 'name')
			$field = 'name_eng';

		$field = ($field !== '') ? 'cards.'.$field : '';

		$order = isset($_GET['order']) ? $_GET['order'] : '';
		$order = ($order !== '') ? $order : 'ASC';

		if ($field === 'power')
			$field = 'power '.$order.', cards.toughness ';


		if (isset($_GET['collection']))
		{
			if ($field === '')
				$field = 'cards.name_eng';

			$arrFields = array_merge($arrFields,array('cards.magic_set'));
			$arrValue  = array_merge($arrValue,array($_GET['collection']));

			if (isset($_GET['type']) && ($_GET['type']!==''))
			{
				$arrFields = array_merge($arrFields,array('cards.colors'));
				if ($_GET['type'] === 'multi')
					$_GET['type'] = ';';
				$arrValue  = array_merge($arrValue,array($_GET['type']));
			}
			if (isset($_GET['rare']) && ($_GET['rare']!==''))
			{
				$arrFields = array_merge($arrFields,array('rarity'));
				$arrValue  = array_merge($arrValue,array($_GET['rare']));
			}
		}

		$dbTblControl = new dbTblControl();
		#$ROWS = getListSearchCards($arrFields,$arrValue,$field,$order,$page,20,$filter);
		$arrWHERE = $dbTblControl->processArraySearch($arrFields,$arrValue);
		if (!$search)
			$Result = searchRows($arrWHERE,array(), $field,$order,$page,50);
		else
			$Result = searchRows(array(),$arrWHERE, $field,$order,$page,50);

		unset($dbTblControl);

		return parseCardsInformation($Result);
	/*
		if (isset($Result['ROWS']))
		{
			$ROWS 	= $Result['ROWS'];
			$TotalP = $Result['TotalPages'];
		}
		else
		{
			$ROWS 	= array();
			$TotalP = 0;
		}

		foreach ($ROWS as $key => &$value)
		{
			switch ($value['colors'])
			{
				case "White": 	$value['css_color'] = 'white'; 		$value['color_name'] = 'Branco'; 	break;
				case "Blue": 	$value['css_color'] = 'blue'; 		$value['color_name'] = 'Azul'; 		break;
				case "Black": 	$value['css_color'] = 'black'; 		$value['color_name'] = 'Preto'; 	break;
				case "Red": 	$value['css_color'] = 'red'; 		$value['color_name'] = 'Vermelho'; 	break;
				case "Green": 	$value['css_color'] = 'green'; 		$value['color_name'] = 'Verde'; 	break;
				case 6: 		$value['css_color'] = 'multi'; 		$value['color_name'] = 'Multicolor';break;
				case "Artifact":$value['css_color'] = 'artifact'; 	$value['color_name'] = 'Artefacto'; break;
				case "Land": 	$value['css_color'] = 'land'; 		$value['color_name'] = 'Terreno'; 	break;
				default: 		$value['css_color'] = 'land'; 		$value['color_name'] = 'Terreno'; 	break;
			}
			$strMana = replaceMana($value['manaCost']);
			
			$value['nosso_price'] = number_format(getPriceDesconto_MagicTuga($value['price']),2);
			$value['rarity']	= ($value['rarity'] !== 'B') ? $value['rarity'] : 'C';
			$value['mana_cost'] = $strMana;
			$value['stock_selector'] = buildSelectStock($value['stock']);
			
			if (($value['power']==='') && ($value['toughness']===''))
				$value['points'] = '';
			else
				$value['points'] = $value['power'].'/'.$value['toughness'];

			$value['jsondata'] = base64_encode(json_encode($value));
		}
		Session::start();
		Session::set('pagination',$TotalP);
		#$auxPages = array();
		#for($i=0; $i<$TotalP; ++$i)
		#{
		#	$auxPages[] = '<a href="">'.($i+1).'</a>';
		#}
		#$_SESSION['pagination'] = '<span class="pagination">'.implode("\n", $auxPages).'</span>';

		return $ROWS;
	*/
	}
	function parseCardsInformation($ResultInput)
	{
		if (isset($ResultInput['ROWS']))
		{
			$ROWS 	= $ResultInput['ROWS'];
			$TotalP = $ResultInput['TotalPages'];
		}
		else
		{
			$ROWS 	= array();
			$TotalP = 0;
		}

		foreach ($ROWS as $key => &$value)
		{
			switch ($value['colors'])
			{
				case "White": 	$value['css_color'] = 'white'; 		$value['color_name'] = 'Branco'; 	break;
				case "Blue": 	$value['css_color'] = 'blue'; 		$value['color_name'] = 'Azul'; 		break;
				case "Black": 	$value['css_color'] = 'black'; 		$value['color_name'] = 'Preto'; 	break;
				case "Red": 	$value['css_color'] = 'red'; 		$value['color_name'] = 'Vermelho'; 	break;
				case "Green": 	$value['css_color'] = 'green'; 		$value['color_name'] = 'Verde'; 	break;
				case 6: 		$value['css_color'] = 'multi'; 		$value['color_name'] = 'Multicolor';break;
				case "Artifact":$value['css_color'] = 'artifact'; 	$value['color_name'] = 'Artefacto'; break;
				case "Land": 	$value['css_color'] = 'land'; 		$value['color_name'] = 'Terreno'; 	break;
				default: 		$value['css_color'] = 'land'; 		$value['color_name'] = 'Terreno'; 	break;
			}
			$strMana = replaceMana($value['manaCost']);
			
			$nosso_preco = getPriceDesconto_MagicTuga($value['price']);
			$value['nosso_price'] 		= number_format($nosso_preco,2);
			$value['rarity']			= ($value['rarity'] !== 'B') ? $value['rarity'] : 'C';
			$value['mana_cost'] 		= $strMana;
			$value['stock_selector'] 	= buildSelectStock($value['stock']);
			$value['text_eng_imgs'] 	= replaceMana($value['text_eng']);
			$value['text_pt_imgs'] 		= replaceMana($value['text_pt']);
			
			$value['css_magic_higher'] 	= ($value['price']>$nosso_preco) ? 'priceMagicTuga bold-light' : 'bold-light';
			$value['css_nosso_lower'] 	= ($nosso_preco<$value['price']) ? 'priceDesconto' : '';


			if (($value['power']==='') && ($value['toughness']===''))
				$value['points'] = '';
			else
				$value['points'] = $value['power'].'/'.$value['toughness'];

			$value['jsondata'] = base64_encode(json_encode($value));
		}
		Session::start();
		Session::set('pagination',$TotalP);

		return $ROWS;
	}
	function paginationCards()
	{
		$Result = array();
		$Result['url_magic_set']= (isset($_GET['collection'])) ? ('&collection='.$_GET['collection']) : '';
		$Result['url_rarity'] 	= (isset($_GET['rare'])) ? ('&rare='.$_GET['rare']) : '';
		$Result['url_type'] 	= (isset($_GET['type'])) ? ('&type='.$_GET['type']) : '';
		$Result['url_field'] 	= (isset($_GET['field'])) ? ('&field='.$_GET['field']) : '';
		$Result['url_order'] 	= (isset($_GET['order'])) ? ('&order='.$_GET['order']) : '';
		$page 					= (isset($_GET['page'])) ? $_GET['page'] : 1;
		$url = implode('', $Result);
		$htmlPages = '';

		Session::start();
		$pages = Session::get('pagination',0);

		if ($pages>=2)
		{
			$auxPages = array();
			for($i=0; $i<$_SESSION['pagination']; ++$i)
			{
				$isPage = ($page==($i+1)) ? 'class="active"':'';
				$auxPages[] = '<a '.$isPage.' href="?op=lista'.$url.'&page='.($i+1).'">'.($i+1).'</a>';
			}
			$htmlPages = '<span class="pagination">'.implode("\n", $auxPages).'</span>';
		}
		return $htmlPages;
	}

	function getPriceDesconto_MagicTuga($preco)
	{
			 if ($preco>=20.00) $preco -= 5.00;
		else if ($preco>=18.00) $preco -= 3.90;
		else if ($preco>=15.00) $preco -= 3.00;
		else if ($preco>=10.00) $preco -= 2.00;
		else if ($preco>=5.00) $preco -= 1.00;
		else if ($preco>=3.00) $preco -= 0.70;
		else if ($preco>=2.00) $preco -= 0.5;
		else if ($preco>=1.40) $preco -= 0.25;
		else if ($preco>=1.00) $preco -= 0.3;
		else if ($preco>=0.70) $preco -= 0.2;
		else if ($preco>=0.60) $preco -= 0.1;
		return $preco;
	}
	function shoppingcart()
	{
		Session::start();
		$carts = Session::get('listcart');

		if ($carts)
		{
			foreach ($carts as $key => &$value)
			{
				$nosso_preco = getPriceDesconto_MagicTuga($value['price']);
				$value['stock_selector'] = buildSelectStock($value['stock'],$value['qtd']);
				$value['total_price'] 	 = number_format($value['qtd']*$nosso_preco,2);
				$value['jsondata'] 		 = base64_encode(json_encode($value));
				$value['price_magictuga']= number_format($value['price'],2);
				$value['price_sum_magictuga'] 	= number_format($value['qtd']*$value['price'],2);
				$value['css_sum_magic_higher'] 	= ($value['price_sum_magictuga']>$value['total_price']) ? 'class="priceMagicTuga bold-light"' : 'class="bold-light"';
				$value['css_magic_higher'] 		= ($value['price']>$nosso_preco) ? 'class="priceMagicTuga bold-light"' : 'class="bold-light"';
				$value['css_nosso_lower'] 		= ($nosso_preco<$value['price']) ? 'class="priceDesconto"' : '';
				$value['css_sum_nosso_lower'] 	= ($value['total_price']<$value['price_sum_magictuga']) ? 'class="priceDesconto"' : '';
				$value['price'] 		 		= number_format($nosso_preco,2);
			}
		}

		if (count($carts)>0)
			return $carts;
		else
			return array();
	}
	function shoppingcardsTotal()
	{
		Session::start();
		$carts = Session::get('listcart');
		$total = 0.0;
		$cards = 0;
		$magituga = 0.0;
		$magictuga_sum = 0.0;
		
		if ($carts)
		{
			foreach ($carts as $key => &$value)
			{
				$nosso_preco = getPriceDesconto_MagicTuga($value['price']);
				$total 			+= $value['qtd']*$nosso_preco;
				$cards 			+= $value['qtd'];
				$magituga 		+= $value['qtd']*$value['price'];
			}
		}
		$total 			= number_format($total,2);
		$magituga 		= number_format($magituga,2);
		$magictuga_sum  = number_format($magictuga_sum,2);

		return array('0'=>array(
				'total_cards'=>$total,
				'len_cards'=>$cards,
				'price_total_magictuga'=>$magituga,
			));
	}
	function shoppingCost()
	{
		Session::start();
		$carts = Session::get('listcart');
		$total = 0.0;

		if ($carts)
		{
			foreach ($carts as $key => &$value)
			{
				$nosso_preco = getPriceDesconto_MagicTuga($value['price']);
				$total += $value['qtd']*$nosso_preco;
			}
		}
		$total = number_format($total+3.90,2);

		return array('0'=>array('total_payment'=>$total));
	}

	/* ******************************************* */
	/* ******************************************* */
	function listCollections()
	{
		$dbTblControl = new dbTblControl();
		$Result = $dbTblControl->findInTable('collections',
									array("collections.id AS 'idRow'",
										  "collections.name_eng AS 'name_eng'",
										  "collections.name_pt AS 'name_pt'",
										  "collections.date_year AS 'date_year'",
										  "collections.total_cards AS 'total_cards'",
										  "collections.magic_set AS 'magic_set'",
										),
									'',
									'',
									array('field'=>'collections.name_eng', 'order'=>'AsC'),
									50,
									array('ENCRYPT','STRING','STRING','STRING','STRING','STRING')
								);
		unset($dbTblControl);

		return $Result;
	}
	/* ******************************************* */
	/* ******************************************* */

	#http://mtgimage.com/
	#http://mtgjson.com/
	#http://archive.wizards.com/Magic/tcg/article.aspx?x=mtg/tcg/products/allproducts
	#http://mtgimage.com/multiverseid/84635.jpg
	#http://magiccards.info/9e/pt.html

?>