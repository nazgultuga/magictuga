<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/magic/dir-vars.php");
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblControl.class.php');
	
	function updatePrices()
	{
		$SQL = 'SELECT 
					cards.name_eng,
					cards.multiverseid,
					cards.price,
					cards.data_alterado,
					collections.name_eng AS "collection"

				FROM cards
				LEFT JOIN collections ON collections.id=cards.id_collection
				WHERE  price=0.0';

		$dbTblControl = new dbTblControl();
		$result = $dbTblControl->getRows($SQL);
		unset($dbTblControl);

		return $result;
	}
	#$SQL_UPDATE = "UPDATE `magictuga`.`cards` SET `price` = '".number_format($price,2)."' WHERE `cards`.`multiverseid` = ".$multiverseid.";";
	#$URL = "http://magictuga.com/index.asp?ant=rap&estado=palavra&val="+$card_name+"&procurar=Procurar";

?>