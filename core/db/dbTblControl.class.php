<?php
		require_once($_SERVER['DOCUMENT_ROOT']."/magic/dir-vars.php");
		require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTable.class.php');
	
	class dbTblControl extends dbTable
	{
		public function __construct()
		{
			parent::__construct();
		}
		public function __destruct()
		{
			parent::__destruct();
		}

		public function INSERTROW($A_POST)
		{
			$Result = false;
			$NEW_ID = false;

			parent::SetFieldsValue($A_POST);
			$Result = parent::Build2RunQuery('INSERT');

			if ($Result)
			{
				$NEW_ID = parent::getInsertedID();
				$NEW_ID = parent::encryptVar($NEW_ID);
			}

			return array('Result'=>$Result, 'NEW_ID'=>$NEW_ID);
		}
		public function UPDATEROW($A_POST,$AarrWhereParams)
		{
			parent::SetFieldsValue($A_POST);
			$Result = parent::Build2RunQuery('UPDATE',$AarrWhereParams);

			return array('Result'=>$Result);
		}
		public function DELETEROW($AarrWhereParams)
		{
			$Result = parent::Build2RunQuery('DELETE',$AarrWhereParams);

			return array('Result'=>$Result);
		}
		public function ListSearch($AsSQL, $AarrFieldName,$AarrValue, $AsFieldToOrder, $iPage=0, $iLimPerPage=10, $AsetFieldsTypeOut, $AbGetTotalRows=false)
		{			
			$sSQLLIKE 	= $this->processArraySearch($AarrFieldName,$AarrValue);
			$sLIMIT 	= parent::build_LIMITStatement($iPage,$iLimPerPage);
			
			$SQL  = $AsSQL;
			$SQL .= $sSQLLIKE;
			$SQL .= " ORDER BY $AsFieldToOrder ";
			$SQL .= $sLIMIT;
		#var_dump($SQL);
			$Result = parent::getResultsOut($SQL,$AsetFieldsTypeOut,$AbGetTotalRows);

			return $Result;
		}
		/* *********************************************************************************** */
		/* ******************************** MongoDB Functions ******************************** */
		/*
		* https://github.com/kevinhao/Mer/blob/master/system/classes/database/set.php
		* http://www.php.net/manual/pt_BR/function.array-keys.php
		* $columns = array_keys($pair);
		* $values = array_values($pair);
		* 
		*/
		/**
		*
		* Builds an SQL stament and performs a SELECT to the table
		*
		* @param string $AsTable 			-> Table Name
		* @param array  $ArrGetFields 		-> Array of Fields Names to retrieve
		* @param array  $AWhereParams 		-> Array of Where Fields
		* @param array  $AarrJoins 			-> Array of JOIN tables to use
		* @param array  $AsOrderBy 			-> Field Name and OrderBy 
		* @param string $AsLimit 			-> Set a Limit to the query
		* @param array  $AOutputAS 			-> Process data retrieve
		* @return array  $ROW 				-> Array of retrieved Rows
		*
		*/
		public function findInTable($AsTable,$ArrGetFields,$AWhereParams,$AarrJoins='', $AsOrderBy='',$AsLimit='',$AOutputAS=false,$AsGroupBy='',$AsTotalRows=false)
		{
			if ($AsTable == false) 		 { $AsTable 		= $this->TableName; }
			if (is_array($ArrGetFields)) { $ArrGetFields 	= implode(', ',$ArrGetFields); }
			if (is_array($AarrJoins)) 	 { $AarrJoins 		= implode(' ',$AarrJoins); }
			if (is_array($AWhereParams)) { $AWhereParams 	= 'WHERE '.$this->processWHEREParams($AWhereParams); }
			if ($AsGroupBy !== '') 		 { $AsGroupBy 		= 'GROUP BY '.$AsGroupBy; }
			if ($AsLimit !== '') 		 { $sLimit 			= 'LIMIT '.$AsLimit; }
			if (is_array($AsOrderBy)) 	 { $AsOrderBy 		= 'ORDER BY '.$AsOrderBy['field'].' '.$AsOrderBy['order']; }
			$SQL = "SELECT $ArrGetFields FROM $AsTable $AarrJoins $AWhereParams $AsGroupBy $AsOrderBy $sLimit;";
	
		#var_dump($SQL);

			$ROW = $this->getRows($SQL);
			if (is_array($AOutputAS))
			{
				parent::outputValues($ROW,$AOutputAS);
			}
			if (stripos($AsLimit,',')!=false)
			{
				$AsLimit = substr($AsLimit, stripos($AsLimit,',')+1);
			}

			if (($AsTotalRows) && (isset($ROW[0])))
			{
				$TotalPages = (int)parent::getTotalPages($SQL,$AsLimit);
				return array('ROWS'=>$ROW,'TotalPages'=>$TotalPages);
			}
			else
				return $ROW;
		}
		
		public function findOne($ArrGetFields,$AarrWhereParams,$AsLimit='0,1')
		{
			return $this->findInTable($this->TableName,$ArrGetFields,$AarrWhereParams,'','',$AsLimit);
		}
		public function find($ArrGetFields,$AarrWhereParams,$AsLimit='')
		{
			return $this->findInTable($this->TableName,$ArrGetFields,$AarrWhereParams,'','',$AsLimit);
		}
		public function getFromTable($AsTable,$AsLimit='0,20')
		{
			return $this->findInTable($AsTable,'*','','','',$AsLimit);
		}
		public function getAll($AsLimit='0,20')
		{
			return $this->getFromTable($this->TableName,$AsLimit);
		}
		public function getOne()
		{
			return $this->getFromTable($this->TableName,'0,1');
		}

		/* *********************************************************************************** */
		/* ************************* SQL QUERY SEARCH ************************* */
		private function processWHEREParams($AWHEREParams)
		{
			$WHERE = '';
			if (is_array($AWHEREParams))
			{
				$sAux = '';
				$aAND = array();
				$aOR = array();
				foreach ($AWHEREParams as $keyComp => &$arrParams)
				{
					foreach ($arrParams as $key => &$arrValue)
					{
						$key = (stripos($key,'.')) ? $key : "`$key`";
						if (is_array($arrValue))
						{
							$arrValue[1] = parent::protectVar($arrValue[1]);
							switch ($arrValue[0])
							{
								case '>': 		{ }
								case '>=':  	{ }
								case '<': 		{ }
								case '<=':  	{ $sAux = $key.$arrValue[0].$arrValue[1]; } break;
								case '=': 		{ $sAux = $key."='".$arrValue[1]."'"; } break;
								case 'like':	{ $sAux = $key." LIKE _utf8 '%".$arrValue[1]."%'"; } break;
								case 'between': { $sAux = $key." BETWEEN ".$arrValue[1]; } break;
								default: 		{ $sAux = $key."='".$arrValue[1]."'"; } break;
							}
						}
						else
						{
							$sAux = $key."='".parent::protectVar($arrValue)."'";
						}
						
						if ($keyComp === 'AND') { $aAND[] = $sAux; }
						else
						if ($keyComp === 'OR')  { $aOR[] = $sAux; }
					}
				}
				$WHERE = (isset($aAND[0]) ? implode(' AND ',$aAND) : '');
				if (isset($aAND[0]) && isset($aOR[0]))
					$WHERE .= ' AND ('.implode(' OR ',$aOR).')';
				else
				if (!isset($aAND[0]) && isset($aOR[0]))
					$WHERE .= implode(' OR ',$aOR);
			}
			return $WHERE;
		}
		public function processArraySearch($AarrFieldName,$AarrValue,$SignalComp='LIKE')
		{
			$Result = array();
			$ccI 	= count($AarrFieldName);
			$ccV 	= count($AarrValue);
			$SignalComp = strtolower($SignalComp);
			
			if ($ccI>0)
			{
				$arrAux = array();
				for ($i=0; $i<$ccI; ++$i)
				{
					$arrAux[$AarrFieldName[$i]] = array($SignalComp,$AarrValue[(($ccV == 1) ? 0 : $i)]);
				}
				$Result = $arrAux;
			}
			
			return $Result;
		}
		public function processLIMIT($AiPage=0,$AiLimPerPage=10)
		{
			$Result = '';
			if (($AiPage !== '') && ($AiLimPerPage !== '') && ($AiLimPerPage >= 1))
			{
				if (($AiPage !== '') && ($AiPage > 1))
				{
					$Result = (($AiPage-1)*$AiLimPerPage).','.($AiLimPerPage);
				}
				else
				{
					$Result = "0,$AiLimPerPage";
				}
			}
			return $Result;
		}
		
/*	
		public function processArraySearch2($AarrFieldName,$AarrValue,$SignalComp1='OR', $SignalComp2='LIKE')
		{
			$Result = '';
			$ccI 	= count($AarrFieldName);
			$ccV 	= count($AarrValue);

			if ($ccI>0)
			{
				if ($ccV == 1)
				{
					for ($i=1; $i<$ccI; ++$i)
					{
						$AarrValue[$i] = $AarrValue[0];
					}
				}
				$Result = ' AND ('.parent::WHEREUPDATE_PARSER($AarrFieldName,$AarrValue," $SignalComp1 ","$SignalComp2",false).') ';
			}
			
			return $Result;
		}
	*/
		/* *********************************************************************************** */
		/* ****************************** FUNCOES GERAIS - HTML ****************************** */
		public static function DATA_EXTENSO($aData)
		{
			$semana= array('Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado');
			$meses = array('Janeiro','Fevereiro','Março','Abril','Maio','Junho',
							'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');
			$dia = date('d',strtotime($aData));
			$mes = date('m',strtotime($aData));
			$ano = date('Y',strtotime($aData));
			$dse = date('w',strtotime($aData));
			return $dia.' de '.$meses[$mes-1].' de '.$ano;
		}
		public static function decodeJSONtoArray($AJSON)
		{
			$jsonData = base64_decode($AJSON);
			$jsonData = urldecode($jsonData);
			$jsonData = utf8_encode($jsonData);
			$jsonData = json_decode($jsonData,true);
			return $jsonData;
		}
	}
?>