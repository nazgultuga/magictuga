<?php
	/* *********************************************************************
	*  Author: Paulo Mota (paulomota2@gmail.com)
	*  Web...: N/A
	*  Name..: dbTable.class.php
	*  Desc..: MySQLi Class
	*  Date..: 05/09/2013
	*  Date-update: 26/02/2014
	*
	*********************************************************************** */
	
	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	#require_once($_SERVER['DOCUMENT_ROOT'].'/header-sessions.php');
	require_once($_SERVER['DOCUMENT_ROOT']."/magic/dir-vars.php");
	require_once(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'encrypt_api.php');
		
	class dbTable
	{
		const HOST_NAME = 'localhost';
		const HOST_USER = 'root';
		const HOST_PASS = 'mysql';
		const DB_NAME   = 'magictuga';
		const PASSCRYPT = 'kd95k0bcgS0Z81nI';
		#xfit_root == MOHJ6frDC2D7j82
		#xfit_user == EJ05LkQnQedfxCB
		
		var $ConnectionOut 		= false;
		public static $conLink	= false;
		public $TableName 			= '';

		var $FieldsInfo 	= array();
		var $FieldsValue 	= array();

		public function __construct($ADBConnection=false)
		{
			$this->ConnectDB($ADBConnection);
		}
		public function __destruct()
		{
			$this->DisconnectDB();
			unset($this->FieldsInfo);
			unset($this->FieldsValue);
		}

		/* ********** SETFUNCTIONS ********** */
		/* ********************************** */
		public function setTableName($vVar)		{ $this->TableName 	 	= $vVar; }
		public function setFieldsInfo($vVar)	{ $this->FieldsInfo  	= $vVar; }
		public function setFieldsValue($vVar)	{ $this->FieldsValue 	= $vVar; }
		/* ********************************** */
		/* ********** GETFUNCTIONS ********** */
		public function getInsertedID() 	{ return self::$conLink->insert_id; }
		public function getDBConnection() 	{ return self::$conLink; }
		private function getFieldsName()
		{
			$Result = array();
			foreach($this->FieldsInfo as $key => $value)
			{
				$Result[] = $key;
			}
			return $Result;
		}
		/* ********************************** */
		private function ERROR_CONNECT()
		{
			die('<b>Erro Ligação MSG:</b> ' .mysqli_connect_error().'<br/>'.
				'<b>Erro Ligação NUM:</b> ' .mysqli_connect_errno().'<br/>');
		}
		private function ERROR_SQL()
		{
			die('<b>Erro SQL MSG:</b> ' .mysqli_error(self::$conLink).'<br/>'.
				'<b>Erro SQL NUM:</b> ' .mysqli_errno(self::$conLink).'<br/>');
		}

		public function ConnectDB($ADBConnection)
		{
			if (self::$conLink == false)
			{
				try
				{
					mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
					
					self::$conLink = new mysqli(self::HOST_NAME,self::HOST_USER,self::HOST_PASS,self::DB_NAME);
					if (mysqli_connect_errno())
					{						
						$this->ERROR_CONNECT();
					}
				}
				catch (Exception $e)
				{
					die($e->getMessage());
				}
			}
		}
		public function DisconnectDB()
		{
			if (!$this->ConnectionOut)
			{
				if (!self::$conLink)
				{
					if (self::$conLink != false)
						$this->ERROR_CONNECT();
				}
				else
				{
					mysqli_close(self::$conLink);
					self::$conLink = false;
				}
			}
		}
		
		/* *********************************************************************************** */
		/* ****************************** PROTECCAO DE STRINGS ******************************* */
		private function ms_escape_string($data)
		{
			$non_displayables = array(
			'/%0[0-8bcef]/', 			# url encoded 00-08, 11, 12, 14, 15
			'/%1[0-9a-f]/', 			# url encoded 16-31
			'/[\x00-\x08]/', 			# 00-08
			'/\x0b/', 					# 11
			'/\x0c/', 					# 12
			'/[\x0e-\x1f]/' 			# 14-31
			);

			foreach ($non_displayables as $regex)
			{
				$data = preg_replace($regex, '', $data);
			}

			return $data;
		}
		private function escape($str)
		{
			$search  = array("\\","\0","\n","\r","\t","\x1a","'",'"');
			$replace = array("\\\\","\\0","\\n","\\r","\\t","\Z","\'",'\"');
			return str_replace($search,$replace,$str);
		}
		public function StringProtect($AValue,$AEncode=true)
		{
			$AValue = trim($AValue);
			$AValue = $this->ms_escape_string($AValue);
			$AValue = $this->escape($AValue);
			if ($AEncode) $AValue = utf8_decode($AValue);
			return $AValue;
		}
		public function protectVar($AValue)
		{
			return $this->StringProtect($AValue);
		}
		
		/* *********************************************************************************** */
		public static function encryptVar($AValue)
		{
			return encrypt($AValue,self::PASSCRYPT);
		}
		public static function decryptVar($AValue)
		{
			return decrypt($AValue,self::PASSCRYPT);
		}
		
		public function br2nl($text)
		{
			return preg_replace('/<br\\s*?\/??>/i', "", $text);
		}
		public function isStrFloat(&$num,$charReplace)
		{
			$num = str_replace($charReplace,' ',$num);
			$num = trim($num);
			$num = str_replace(',','.',$num);
			$num = (float)$num;
			return (is_float($num)) ? true : false;
		}
		public function isStrDate(&$str)
		{
			$stamp = strtotime( $str );
			$result = false;
			if (!is_numeric($stamp))
			{
				$result = false;
			}
			$month = date( 'm', $stamp );
			$day   = date( 'd', $stamp );
			$year  = date( 'Y', $stamp );

			if (checkdate($month, $day, $year))
			{
				$str    = $year.'-'.$month.'-'.$day;
				$result = true;
			}

			return $result;
		}
		public function setAsInteger($AValue)
		{
			preg_match_all('!\d+!', $AValue, $AValue);
			return implode('',$AValue[0]);
		}
		public function setAsFloat($AValue)
		{
			preg_match_all('!\d+\.*\d*!', $AValue, $AValue);
			return implode('',$AValue[0]);
		}
		public function setAsDate($AValue)
		{
			if (!$this->isStrDate($AValue))
			{
				$AValue = '';
			}
			return $AValue;
		}
		public function getAsString($AValue)
		{
			return utf8_encode($AValue);
		}
		public function getAsMemo($AValue)
		{
			return nl2br($this->getAsString($AValue));
		}
		public function getAsInteger()
		{
			preg_match_all('!\d+!', $AValue, $AValue);
			return implode('',$AValue[0]);
		}
		public function getAsFloat($AValue, $ADecimals=2)
		{
			return number_format($AValue, $ADecimals, '.', '');
		}
		public function getAsDate($AValue, $AOtherDate, $AInput=false)
		{
			$sType = ($AInput) ? 'Y-m-d' : 'd-m-Y';
			$AValue = str_replace('0000-00-00','',$AValue);
			$AValue = ($AValue !== '') ? date($sType,strtotime($AValue)) : $AOtherDate;
			return $AValue;
		}
		
		/* ************************* PAGINACAO ************************* */
		public function getTotalPages($ASQL,$AiLimPerPage=20)
		{
			$Result = 0;
			$pos 	= stripos($ASQL,'FROM');
			$AuxSQL = substr($ASQL,$pos);
			$pos 	= stripos($AuxSQL,'LIMIT');
			$AuxSQL = substr($AuxSQL,0,$pos);
			$AuxSQL = "SELECT COUNT(*) AS 'total' $AuxSQL";
			
			$ROW = $this->getRows($AuxSQL);
			if (isset($ROW[0]))
			{
				$total = $ROW[0]['total'];
				$AiLimPerPage = ($AiLimPerPage <= 0) ? 1 : $AiLimPerPage;
				$Result = ceil($total/$AiLimPerPage);
				#http://php.net/manual/en/function.round.php
			}
			return $Result;
		}
		/* *********************************************************************************** */
		#http://dadomingues.blogspot.pt/2008/10/select-ignorando-acento.html
		public function WHEREUPDATE_PARSER($vFieldsName,$vFieldsValue,$strGlue,$sSignal='=',$bProtect=true)
		{
			$sP 		= ($bProtect === true) ? '`' : '';
			$arrWHERE 	= array();

			if ($sSignal === '=')
			{
				$sBegin = "'";
				$sEnd 	= "' ";
			}
			else
			if ($sSignal === 'LIKE')
			{
				$sBegin = " _utf8 '%";
				$sEnd 	= "%' ";
			}

			foreach($vFieldsValue as $key => &$value)
			{
				if (is_numeric($key))
				{
					$value 	= $vFieldsValue[$key];
					$key 	= $vFieldsName[$key];
				}
				if ($value !== false)
				{
					$arrWHERE[] = $sP.$key.$sP.' '.$sSignal.' '.$sBegin.$value.$sEnd;
				}
			}

			return implode($strGlue,$arrWHERE);
		}
		public function PARAMS_PARSER($AarrWhereParams,$strGlue=' AND ')
		{
			$arrWHERE 	= array();
			$sP 		= '`';
			$sSignal 	= '=';
			$sBegin 	= "'";
			$sEnd 		= "' ";

			foreach($AarrWhereParams as $key => &$value)
			{
				if ($value !== false)
				{
					$arrWHERE[] = $sP.$key.$sP.' '.$sSignal.' '.$sBegin.$value.$sEnd;
				}
			}

			return implode($strGlue,$arrWHERE);
		}
		##SELECT BUILD
		public function build_ORStatments($Asqlfield,$AArray)
		{
			$SQLAux = array();
			foreach ($AArray as $key => $value)
			{
				$SQLAux[] = $Asqlfield.'="'.$this->decryptVar($value).'"';
			}
			return ' AND ('.implode(' OR ',$SQLAux).')';
		}
		#$sLIMIT   = ((isset($iPage)) && ($iPage!=='') && ($iPage > 1)) ? ' LIMIT '.(($iPage-1)*$iLimPerPage).','.($iLimPerPage) : (' LIMIT 0,'.$iLimPerPage) ;
		public function build_LIMITStatement($AiPage=0,$AiLimPerPage=10)
		{
			$sLIMIT = '';
			if (($AiPage !== '') && ($AiLimPerPage !== '') && ($AiLimPerPage >= 1))
			{
				if (($AiPage !== '') && ($AiPage > 1))
				{
					$sLIMIT = (($AiPage-1)*$AiLimPerPage).','.($AiLimPerPage);
				}
				else
				{
					$sLIMIT = "0,$AiLimPerPage";
				}

				$sLIMIT = " LIMIT $sLIMIT";
			}

			return $sLIMIT;
		}
		/* *********************************************************************************** */
		public function outputValues(&$aValues, $arrFieldsType)
		{
			foreach ($aValues as $keyI => &$arrValue)
			{
				$k=0;
				foreach ($arrValue as $key => &$value)
				{
					if ($value !== '')
					{
						switch($arrFieldsType[$k])
						{
							case 'STRING'	: { $value = $this->getAsString($value); 					} break;
							case 'STRINGINT': {} break;
							case 'STRINGNEW': {} break;
							case 'INTEGER'	: { $value = wordwrap($value,3,' ',true); 					} break;
							case 'FLOAT'	: { $value = $this->getAsFloat($value); 					} break;
							case 'DATE'		: { $value = $this->getAsDate($value,'0000-00-00'); 		} break;
							case 'DATEINPUT': { $value = $this->getAsDate($value,'',true); 				} break;
							case 'DATEINPUTNOW': { $value = $this->getAsDate($value,date('Y-m-d'),true);} break;
							case 'DATETIME' : {} break;
							case 'AGE' 		: { #http://stackoverflow.com/questions/3380990/php-calculate-persons-current-age
												$today = new DateTime();
												$diff = $today->diff(new DateTime($value));
												
												#printf('%d years, %d month, %d days', $diff->y, $diff->m, $diff->d);
												$value = $diff->y;
											} break;
							case 'MEMO' 	: { $value = $this->getAsMemo($value); 						} break;
							case 'ENCRYPT' 	: { $value = $this->encryptVar($value); 					} break;
						}
					}
					$k++;
				}
			}
		}
		#se (_POST !== false) 
		#se (_fieldrequired === false)
		#se (_POST !== "")
		private function validateValues()
		{
			foreach($this->FieldsValue as $key => &$value)
			{
				if (($value !== false) && ($value !== ''))
				{
					$value = $this->StringProtect($value);
					switch($this->FieldsInfo[$key]['type'])
					{
						case 'STRING'	: {} break;
						case 'STRINGINT': { $value = $this->setAsInteger($value); 	} break;
						case 'STRINGNEW': { $value = str_replace(' ','',$value); 	} break;
						case 'INTEGER'	: { 
											if ($value !== "NULL") # IF field name == 'id'
											{
												$value = $this->setAsInteger($value);
											}
										  } break;
						case 'FLOAT'	: { $value = $this->setAsFloat($value); 	} break;
						case 'DATE'		: { $value = $this->setAsDate($value); 		} break;
						case 'DATETIME'	: {} break;
						case 'MEMO'		: {} break;
						case 'DECRYPT'	: { $value = $this->decryptVar($value); 	} break;
						case 'MD5'		: { $value = $this->encryptVar(md5($value));} break;
					}
				}
				else
				if (($value === '') && ($this->FieldsInfo[$key]['required'] === true))
				{
					return false;
				}
			}

			return true;
		}
		/* *********************************************************************************** */
		public function SQL_SELECT($strTable, $arrFieldName, $vFieldsNameWHERE, $vFieldsValueWHERE, $strLimit = '0,1', $strOther = '')
		{
			$arrFieldName = implode(', ',$arrFieldName);
			$WHERE	= $this->WHEREUPDATE_PARSER($vFieldsNameWHERE,$vFieldsValueWHERE,' AND ');
			$LIMIT	= ($strLimit !== '') ? 'LIMIT '.$strLimit : '';
			$SQL	= "SELECT $arrFieldName FROM $strTable WHERE $WHERE $strOther $LIMIT;";

			return $this->getRows($SQL);
		}
		/* *********************************************************************************** */
		public function ExecSQL($vSQL)
		{
			$vSQL = $this->ms_escape_string($vSQL);
			$Result = self::$conLink->query($vSQL) or $this->ERROR_SQL();
			
			return $Result;
		}
		public function BuildSQL($sAction,$AarrWhereParams=false)
		{
			switch($sAction)
			{
				case 'INSERT': 	{
									$arrFields 	= $this->getFieldsName();
									$sqlFields 	= "`".implode("`,`",$arrFields)."`";
									$sqlValues 	= implode("','",$this->FieldsValue);
									$SQL 		= "INSERT INTO $this->TableName ($sqlFields) VALUES ('$sqlValues');";
								} break;
				case 'UPDATE': 	{
									$FValues 	= $this->PARAMS_PARSER($this->FieldsValue,' , ');
									$WHERE		= $this->PARAMS_PARSER($AarrWhereParams,' AND ');
									$SQL 		= "UPDATE $this->TableName SET $FValues WHERE $WHERE;";
								} break;
				case 'DELETE': 	{
									$WHERE 		= $this->PARAMS_PARSER($AarrWhereParams,' AND ');
									$SQL 		= "DELETE FROM $this->TableName WHERE $WHERE;";
								} break;
			}
	#var_dump($SQL);
			return $SQL;
		}
		public function getRows($vSQL)
		{
			$aARRAY = array();

			$res = $this->ExecSQL($vSQL);
			if (($res) && (self::$conLink->affected_rows > 0))
			{
				while ($row = $res->fetch_assoc())
				{
					$aARRAY[] = $row;
				}
				mysqli_free_result($res);
			}

			return $aARRAY;
		}
		public function Build2RunQuery($sAction,$AarrWhereParams=false)
		{
			$Result = false;
			if ($this->validateValues())
			{
				$SQL 	= $this->BuildSQL($sAction,$AarrWhereParams);
				$Result = $this->ExecSQL($SQL);
			}
			return $Result;
		}
		public function getResultsOut($sSQL,$arrFieldsTypeOut,$AbGetTotalRow=false,$iLimPerPage=20)
		{
			$EXIST 	= false;
			$ROW 	= $this->getRows($sSQL);
			$TotalP = 0;
			
			if ($EXIST = isset($ROW[0]))
			{
				$this->outputValues($ROW,$arrFieldsTypeOut);
			}
			if ($EXIST && $AbGetTotalRow)
			{
				$TotalP = (int)$this->getTotalPages($sSQL,$iLimPerPage);
			}
			
			return array('ROW'=>$ROW, 'EXIST'=>$EXIST, 'TotalPage'=>$TotalP);
		}
		public function checkIfExist($ATableName,$ArrSELECTFields,$ArrWHEREFieldsName,$AarrWHEREFieldsValue)
		{
			$ROW = $this->SQL_SELECT($ATableName,$ArrSELECTFields,$ArrWHEREFieldsName,$AarrWHEREFieldsValue);
			return (isset($ROW[0])) ? $ROW : false;
		}
	}
?>