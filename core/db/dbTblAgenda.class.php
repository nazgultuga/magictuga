<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/magic/dir-vars.php");
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblControl.class.php');
	
	class dbTblAgenda extends dbTblControl
	{
		public $ID_MAIN_ENCRYPTED = '';
		public $ID_MAIN_DECRYPTED = '';
		public $ID_USER 		  = '';
		public $ID_TREINADOR 	  = '';
		public $ID_ATLETA 	 	  = '';

		public function __construct()
		{
			parent::__construct();

			$this->setTableName("agenda");
			$this->setFieldsInformation();
		}
		public function __destruct()
		{
			parent::__destruct();
			unset($this->ID_USER);
			unset($this->ID_MAIN_ENCRYPTED);
			unset($this->ID_MAIN_DECRYPTED);
			unset($this->ID_TREINADOR);
			unset($this->ID_USER);
			unset($this->ID_ATLETA);
		}

		public function setFieldsInformation()
		{
			parent::setFieldsInfo(array(
										'id' 				=> array('type'=>'INTEGER', 'required'=>true),
										'id_treinador' 		=> array('type'=>'INTEGER', 'required'=>true),
										'id_atleta' 		=> array('type'=>'STRING', 'required'=>true),
										'descricao' 		=> array('type'=>'STRING', 'required'=>true),
										'cliente_nome' 		=> array('type'=>'STRING', 'required'=>false),
										'data' 				=> array('type'=>'DATE', 'required'=>true),
										'hora_inicio' 		=> array('type'=>'TIME', 'required'=>true),
										'hora_fim' 			=> array('type'=>'TIME', 'required'=>true),
										'tipo' 				=> array('type'=>'INTEGER', 'required'=>true),
										'observacoes' 		=> array('type'=>'STRING', 'required'=>false),
										'avaliacao' 		=> array('type'=>'INTEGER', 'required'=>false),
										'concluido' 		=> array('type'=>'INTEGER', 'required'=>false),
										'id_user_created' 	=> array('type'=>'INTEGER', 'required'=>true),
										'id_user_edited' 	=> array('type'=>'INTEGER', 'required'=>true),
										'data_criacao' 		=> array('type'=>'DATETIME', 'required'=>true),
										'data_alteracao' 	=> array('type'=>'DATETIME', 'required'=>true),
										'enabled' 			=> array('type'=>'INTEGER', 'required'=>true)
									));
		}

		public function set_IDMAIN($AsVar)
		{
			$this->ID_MAIN_ENCRYPTED = $AsVar;
			$this->ID_MAIN_DECRYPTED = ($AsVar !== '') ? parent::decryptVar($AsVar) : '';
		}
		public function set_IDUSER($AsVar) 		{ $this->ID_USER = $AsVar; }
		public function set_IDTREINADOR($AsVar) { $this->ID_TREINADOR = $AsVar; }
		public function set_IDATLETA($AsVar) 	{ $this->ID_ATLETA = $AsVar; }

		public function INSERT($A_POST)
		{
			return parent::INSERTROW(array(
										'id' 				=> 'NULL',
										'id_treinador' 		=> $this->ID_TREINADOR,
										'id_atleta' 		=> $this->ID_ATLETA,
										'descricao' 		=> $A_POST['descricao'],
										'cliente_nome' 		=> isset($A_POST['nome_cliente']) ? $A_POST['nome_cliente'] : '',
										'data' 				=> $A_POST['data'],
										'hora_inicio' 		=> $A_POST['hora_inicio'],
										'hora_fim' 			=> $A_POST['hora_fim'],
										'tipo' 				=> $A_POST['tipo'],
										'observacoes' 		=> $A_POST['observacoes'],
										'avaliacao' 		=> $A_POST['avaliacao'],
										'concluido' 		=> $A_POST['concluido'],
										'id_user_created' 	=> $this->ID_USER,
										'id_user_edited' 	=> $this->ID_USER,
										'data_criacao' 		=> date('Y-m-d H:i:s'),
										'data_alteracao' 	=> date('Y-m-d H:i:s'),
										'enabled' 			=> '0')
									);
		}
		public function UPDATE($A_POST)
		{
			return parent::UPDATEROW(array(
										'id' 				=> false,
										'id_treinador' 		=> $this->ID_TREINADOR,
										'id_atleta' 		=> $this->ID_ATLETA,
										'descricao' 		=> $A_POST['descricao'],
										'cliente_nome' 		=> isset($A_POST['nome_cliente']) ? $A_POST['nome_cliente'] : '',
										'data' 				=> $A_POST['data'],
										'hora_inicio' 		=> $A_POST['hora_inicio'],
										'hora_fim' 			=> $A_POST['hora_fim'],
										'tipo' 				=> $A_POST['tipo'],
										'observacoes' 		=> $A_POST['observacoes'],
										'avaliacao' 		=> $A_POST['avaliacao'],
										'concluido' 		=> $A_POST['concluido'],
										'id_user_created' 	=> false,
										'id_user_edited' 	=> $this->ID_USER,
										'data_criacao' 		=> false,
										'data_alteracao' 	=> date('Y-m-d H:i:s'),
										'enabled' 			=> false
										),
									array('id'=>$this->ID_MAIN_DECRYPTED));
		}
		public function DELETE()
		{
			return parent::DELETEROW(array('id'=>$this->ID_MAIN_DECRYPTED));
		}
		public function getListSearch($AarrFieldName,$AarrValue, $AsFieldToOrder,$OrderBy='ASC', $iPage=0,$iLimPerPage=10, $AbFilter=true)
		{
			$arrWHERE = parent::processArraySearch($AarrFieldName,$AarrValue,((!$AbFilter) ? '=' : 'LIKE'));
			$sLIMIT = parent::processLIMIT($iPage,$iLimPerPage);
			
			$WhereArray = array();
			$WhereArray["agenda.enabled"] 		= 0;
			$WhereArray["agenda.id_treinador"] 	= $this->ID_TREINADOR;
			$WhereArray["agenda.id_atleta"] 	= $this->ID_ATLETA;
			if ($this->ID_ATLETA !== '')
				$WhereArray["agenda.id"] = $this->ID_MAIN_DECRYPTED;
			
			if ($AsFieldToOrder === '')
				$AsFieldToOrder = 'agenda.data';
			
			$Rows = parent::findInTable('agenda',
										array("agenda.id AS 'idRow'",
											  "agenda.id_treinador AS 'id_treinador'",
											  "agenda.id_atleta AS 'id_atleta'",
											  "agenda.descricao AS 'descricao'",
											  "agenda.cliente_nome AS 'cliente_nome'",
											  "agenda.data AS 'data'",
											  "agenda.hora_inicio AS 'hora_inicio'",
											  "agenda.hora_fim AS 'hora_fim'",
											  "agenda.tipo AS 'tipo'",
											  "agenda.observacoes AS 'observacoes'",
											  "agenda.avaliacao AS 'avaliacao'",

											  "atletas.nome AS 'nome'",
											  "atletas.apelido AS 'apelido'"
											),
										array('AND' => $WhereArray,
											  'OR' => $arrWHERE
											),
										'LEFT JOIN atletas ON atletas.id=agenda.id_atleta',
										array('field'=>$AsFieldToOrder, 'order'=>$OrderBy),
										$sLIMIT,
										array('ENCRYPT','ENCRYPT','ENCRYPT','STRING','STRING','STRING','STRING','STRING','STRING','STRING',
												'STRING','STRING')
										);
			return $Rows;
		}
		public function getData()
		{
			$Result = false;
			
			if ($this->ID_MAIN_DECRYPTED != '')
			{
				$Result = $this->getListSearch(array(),array(),'','ASC',0,1,false);
			}
			
			return $Result;
		}
		public function getBetweenData($ADataInicio,$ADataFim,$iPage=0,$iLimPerPage=40)
		{
			$sLIMIT = parent::processLIMIT($iPage,$iLimPerPage);

			$arrWHERE = array();
			$WhereArray = array();
			$WhereArray["agenda.enabled"] 		= 0;
			$WhereArray["agenda.id_treinador"] 	= $this->ID_TREINADOR;
			#$WhereArray["agenda.id_atleta"] 	= $this->ID_ATLETA;

			$WhereArray["agenda.data"] 			= array('>='," '".$ADataInicio."' AND agenda.data <= '".$ADataFim."' ");
			
			$OrderBy = 'ASC, agenda.hora_inicio ASC';
			$AsFieldToOrder = 'agenda.data';
			
			$Rows = parent::findInTable('agenda',
										array("agenda.id AS 'idRow'",
											  "agenda.id_treinador AS 'id_treinador'",
											  "agenda.id_atleta AS 'id_atleta'",
											  "agenda.descricao AS 'descricao'",
											  "agenda.cliente_nome AS 'cliente_nome'",
											  "agenda.data AS 'data'",
											  "agenda.hora_inicio AS 'hora_inicio'",
											  "agenda.hora_fim AS 'hora_fim'",
											  "agenda.tipo AS 'tipo'",
											  "agenda.observacoes AS 'observacoes'",
											  "agenda.avaliacao AS 'avaliacao'",

											  "atletas.nome AS 'nome'",
											  "atletas.apelido AS 'apelido'"
											),
										array('AND' => $WhereArray,
											  'OR' => $arrWHERE
											),
										'LEFT JOIN atletas ON atletas.id=agenda.id_atleta',
										array('field'=>$AsFieldToOrder, 'order'=>$OrderBy),
										$sLIMIT,
										array('ENCRYPT','ENCRYPT','ENCRYPT','STRING','STRING','STRING','STRING','STRING','STRING','STRING',
												'STRING','STRING')
										);
			return $Rows;
		}
	}
?>