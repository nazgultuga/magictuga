<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/magic/dir-vars.php");
	require_once(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblControl.class.php');
	
	class dbTblCardInformation extends dbTblControl
	{
		public function __construct()
		{
			parent::__construct();

			$this->setTableName("card_information");
			$this->setFieldsInformation();
		}
		public function __destruct()
		{
			parent::__destruct();
		}

		public function setFieldsInformation()
		{
			parent::setFieldsInfo(array(
										'id' 				=> array('type'=>'INTEGER', 'required'=>true),
										'id_collection' 	=> array('type'=>'INTEGER', 'required'=>true),
										'id_cardtype' 		=> array('type'=>'INTEGER', 'required'=>true),
										'name_eng' 			=> array('type'=>'STRING', 'required'=>true),
										'name_pt' 			=> array('type'=>'STRING', 'required'=>true),
										'color' 			=> array('type'=>'INTEGER', 'required'=>true),
										'mana_cost' 		=> array('type'=>'STRING', 'required'=>false),
										'points' 			=> array('type'=>'STRING', 'required'=>false),
										'rare' 				=> array('type'=>'INTEGER', 'required'=>true),
										'stock' 			=> array('type'=>'INTEGER', 'required'=>true),
										'price' 			=> array('type'=>'FLOAT', 'required'=>true),
										'description_eng' 	=> array('type'=>'STRING', 'required'=>true),
										'description_pt' 	=> array('type'=>'STRING', 'required'=>true),
										'number' 			=> array('type'=>'INTEGER', 'required'=>true),
										'image_url' 		=> array('type'=>'STRING', 'required'=>true),
									));
		}

		public function INSERT($A_POST)
		{
			return parent::INSERTROW(array(
										'id' 				=> 'NULL',
										'id_collection' 	=> $A_POST['id_collection'],
										'id_cardtype' 		=> $A_POST['id_cardtype'],
										'name_eng' 			=> $A_POST['name_eng'],
										'name_pt' 			=> $A_POST['name_pt'],
										'color' 			=> $A_POST['color'],
										'mana_cost' 		=> $A_POST['mana_cost'],
										'points' 			=> $A_POST['points'],
										'rare' 				=> $A_POST['rare'],
										'stock' 			=> $A_POST['stock'],
										'price' 			=> $A_POST['price'],
										'description_eng' 	=> $A_POST['description_eng'],
										'description_pt' 	=> $A_POST['description_pt'],
										'number' 			=> $A_POST['number'],
										'image_url' 		=> $A_POST['image_url'],
										)
									);
		}
		public function UPDATE($A_POST,$APOST_ID)
		{
			return parent::UPDATEROW(array(
										'id' 				=> false,
										'id_collection' 	=> $A_POST['id_collection'],
										'id_cardtype' 		=> $A_POST['id_cardtype'],
										'name_eng' 			=> $A_POST['name_eng'],
										'name_pt' 			=> $A_POST['name_pt'],
										'color' 			=> $A_POST['color'],
										'mana_cost' 		=> $A_POST['mana_cost'],
										'points' 			=> $A_POST['points'],
										'rare' 				=> $A_POST['rare'],
										'stock' 			=> $A_POST['stock'],
										'price' 			=> $A_POST['price'],
										'description_eng' 	=> $A_POST['description_eng'],
										'description_pt' 	=> $A_POST['description_pt'],
										'number' 			=> $A_POST['number'],
										'image_url' 		=> $A_POST['image_url'],
										),
									array('id'=>$APOST_ID));
		}
		public function DELETE($APOST_ID)
		{
			return parent::DELETEROW(array('id'=>$APOST_ID));
		}
		public function getListSearch($AarrFieldName,$AarrValue, $AsFieldToOrder,$OrderBy='ASC', $iPage=0,$iLimPerPage=10, $AbFilter=true)
		{
			$arrWHERE = parent::processArraySearch($AarrFieldName,$AarrValue,((!$AbFilter) ? '=' : 'LIKE'));
			$sLIMIT = parent::processLIMIT($iPage,$iLimPerPage);
			
			$WhereArray = array();

			if ( (!isset($WhereArray[0])) && (!isset($arrWHERE[0])) )
			{
				$arraySearch = '';
			}
			else
			{
				$arraySearch = array('AND' => $WhereArray,
									'OR' => $arrWHERE);
			}

			if ($AsFieldToOrder === '')
				$AsFieldToOrder = 'card_information.name_eng';
			
			$Rows = parent::findInTable('card_information',
										array("card_information.id AS 'idRow'",
											  "card_information.id_collection AS 'id_collection'",
											  "card_information.id_cardtype AS 'id_cardtype'",

											  "card_information.name_eng AS 'name_eng'",
											  "card_information.name_pt AS 'name_pt'",
											  "card_information.color AS 'color'",
											  "card_information.mana_cost AS 'mana_cost'",
											  "card_information.points AS 'points'",
											  "card_information.rare AS 'rare'",
											  "card_information.stock AS 'stock'",
											  "card_information.price AS 'price'",

											  "card_information.description_eng AS 'description_eng'",
											  "card_information.description_pt AS 'description_pt'",
											  "card_information.number AS 'number'",
											  "card_information.image_url AS 'image_url'",

											  "collections.name AS 'collection_name'",
											  "collections.magic_set AS 'magic_set'",
											  "card_type.name_eng AS 'cardtype_name_eng'",
											),
										$arraySearch,
										array('LEFT JOIN collections ON collections.id=card_information.id_collection',
											  'LEFT JOIN card_type ON card_type.id=card_information.id_cardtype'
											),
										array('field'=>$AsFieldToOrder, 'order'=>$OrderBy),
										50,
										array('ENCRYPT','ENCRYPT','ENCRYPT',
											  'STRING','STRING',
											  'INTEGER','STRING','STRING','INTEGER','INTEGER','FLOAT',
											  'STRING','STRING','INTEGER','STRING',
											  'STRING','STRING','STRING')
										);
			return $Rows;
		}
		public function getData()
		{
			return $this->getListSearch(array(),array(),'','ASC');
		}
	}
?>