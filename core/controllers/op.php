<?php

	class op extends RouterController
	{
		public static function index()
		{
			echo 'Em desenvolvimento...';
		}
		public static function login()
		{
			if (isset($_POST['ajax']) && ($_POST['ajax'] == true))
			{
				include(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'logincheck.php');
			}
			else
			{
				$path = SETPATH('ROOT','PATH_APP_UI_LOGIN');
				$objATPL = new TobjAutoTemplate();
				echo $objATPL->tpl_page($path.'index.php',true,false);
			}
		}
		public static function logout()
		{
			include(SETPATH('ROOT','PATH_APP_CORE_SCRIPTS').'logout.php');
		}
		public static function dashboard()
		{
			include(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblExerciciosCategoria.class.php');
			$tbl = new dbTblExerciciosCategoria();
			$ROWS = $tbl->getCategorias(array(),array(),'','ASC',0,50,false);
			unset($tbl);

			$jsonCategorias = base64_encode(json_encode($ROWS));
			function Categorias()
			{
				return func_get_args();
			}

			$path = SETPATH('ROOT','PATH_APP_UI_DASHBOARD');
			$objATPL = new TobjAutoTemplate();
			$objATPL->setVar('{jsonCategorias}',$jsonCategorias);
			echo $objATPL->tpl_page($path.'novo_exercicio.php',true,false);
		}
		public static function plano_treino($tab='',$tabName='')
		{
			$path = SETPATH('ROOT','PATH_APP_UI_PLANO_TREINO');
			$actTab1 = '';
			$actTab2 = '';
			
			switch( (isset($tabName) ? $tabName : '') )
			{
				case 'geral':
					{
						$filename = 'geral.php';
						$actTab1 = 'active';
						require_once(SETPATH('ROOT','PATH_APP_UI_PLANO_TREINO').'json-planocriar.php');
					} break;
				case 'criar':
					{
						$filename = 'criar.php';
						$actTab2 = 'active';
						require_once(SETPATH('ROOT','PATH_APP_UI_PLANO_TREINO').'json-planocriar.php');
					} break;
				case 'criar2': 	{ $filename = 'criar_passo2.php'; 	$actTab2 = 'active'; } break;
				default: 		{ $filename = 'geral.php'; 			$actTab1 = 'active'; } break;
			}
			
			$objATPL = new TobjAutoTemplate();
			$objATPL->setVar('{actTab1}', $actTab1);
			$objATPL->setVar('{actTab2}', $actTab2);
			$objATPL->setVar('{actTab3}', $actTab3);
			$objATPL->setVar('{TAB_SECTION}', $objATPL->tpl_page($path.$filename,true,false));
			$objATPL->setVar('{SEC_SECTION}', $SEC_SECTION);
			echo $objATPL->tpl_page($path.'index.php',true,false);
			
		}
		public static function agenda()
		{
			$path = SETPATH('ROOT','PATH_APP_UI_TOOLS');
			$file = 'agenda.php';
			
			$today = date('Y-m-d');
			$ano = date('Y');
			$mes = date('m');
			$dia = date('d');
			$diaSemana = date('w');
			$lastDay = date('t');
			$k = 1;
			$nextMonth = ($mes<10) ? '0'.($mes+1) : $mes+1;
			$nextYear  = ($nextMonth>12) ? ($ano+1) : $ano;
			$nextMonth = ($nextMonth>12) ? '01' : $nextMonth;

			$datas = array();
			$semana = array();

			function getDiaZeros($value)
			{
				return ($value<10) ? '0'.$value : $value;
			}
			for($i=0; $i<=6; ++$i)
			{
				if ($i==$diaSemana)
				{
					$semana[$i] = $dia;
					$datas[$i] = $ano.'-'.$mes.'-'.getDiaZeros($semana[$i]);
				}
				else
				if ($i<$diaSemana)
				{
					#$semana[$i] = ($i==0) ? ($dia+6) : ($dia-$i);
					#$semana[$i] = ($i==0) ? ($dia+6) : ($dia-($diaSemana-$i));
					##$semana[$i] = ($i==0) ? ($dia+(7-$diaSemana)) : ($dia-($diaSemana-$i));
					// DOMINGO
					if ($i==0)
					{
						$semana[$i] = getDiaZeros($dia+(7-$diaSemana));
						$datas[$i] = $ano.'-'.$mes.'-'.$semana[$i];
						#$k++;
					}
					else
					{
						if (($dia-($diaSemana-$i))<=0)
						{
							$mesOld = getDiaZeros($mes-1);
							$lastOldDay = date('t',strtotime(date('Y').'-'.$mesOld.'-01'))+1;
							$semana[$i] = getDiaZeros($lastOldDay-($diaSemana-$i));
							$datas[$i] = $ano.'-'.$mesOld.'-'.$semana[$i];
						}
						else
						{
							$semana[$i] = getDiaZeros($dia-($diaSemana-$i));
							$datas[$i] = $ano.'-'.$mes.'-'.$semana[$i];
						}
					}

					if ($semana[$i]>$lastDay)
					{
						if ($i==0)
						{
							#$semana[$i] = getDiaZeros(($dia+6)-$lastDay);
							$semana[$i] = getDiaZeros(($dia+(7-$diaSemana))-$lastDay);
							$datas[$i] = $nextYear.'-'.$nextMonth.'-'.$semana[$i];
							#$k++;
						}
						else
						{
							$semana[$i] = getDiaZeros($k++);
							$datas[$i] = $nextYear.'-'.$nextMonth.'-'.$semana[$i];
						}
					}
				}
				else
				if ($i>$diaSemana)
				{
					$semana[$i] = getDiaZeros($dia+($i-$diaSemana));
					$datas[$i] = $ano.'-'.$mes.'-'.$semana[$i];
					if ($semana[$i]>$lastDay)
					{
						$semana[$i] = getDiaZeros($k++);
						$datas[$i] = $nextYear.'-'.$nextMonth.'-'.$semana[$i];
					}
				}
			}

			$objATPL = new TobjAutoTemplate();
			$objATPL->setVar('{segunda}', 	$semana[1]);
			$objATPL->setVar('{terca}', 	$semana[2]);
			$objATPL->setVar('{quarta}', 	$semana[3]);
			$objATPL->setVar('{quinta}', 	$semana[4]);
			$objATPL->setVar('{sexta}', 	$semana[5]);
			$objATPL->setVar('{sabado}', 	$semana[6]);
			$objATPL->setVar('{domingo}', 	$semana[0]);
			
			$objATPL->setVar('{segundaToday}', 	($diaSemana == 1) ? 'today' : '');
			$objATPL->setVar('{tercaToday}', 	($diaSemana == 2) ? 'today' : '');
			$objATPL->setVar('{quartaToday}', 	($diaSemana == 3) ? 'today' : '');
			$objATPL->setVar('{quintaToday}', 	($diaSemana == 4) ? 'today' : '');
			$objATPL->setVar('{sextaToday}', 	($diaSemana == 5) ? 'today' : '');
			$objATPL->setVar('{sabadoToday}', 	($diaSemana == 6) ? 'today' : '');
			$objATPL->setVar('{domingoToday}', 	($diaSemana == 0) ? 'today' : '');
			
			echo $objATPL->tpl_page($path.$file,true,false);
		}
		public static function agenda_novo()
		{
			$path = SETPATH('ROOT','PATH_APP_UI_TOOLS');
			$file = 'agenda-novo-evento.php';

			$objATPL = new TobjAutoTemplate();
			echo $objATPL->tpl_page($path.$file,true,false);
		}
	}
	
?>