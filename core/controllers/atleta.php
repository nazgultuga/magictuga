<?php

	class atleta extends RouterController
	{
		public static function novo()
		{
			$path = SETPATH('ROOT','PATH_APP_UI_ATLETA');
			$file = 'user_novo.php';
			
			$objATPL = new TobjAutoTemplate();
			$HTML = $objATPL->tpl_page($path.$file,true,false);
			
			unset($objATPL);
			echo $HTML;
		}
		public static function editar()
		{
			$path = SETPATH('ROOT','PATH_APP_UI_ATLETA');
			$file = 'user_editar.php';
			$file = 'user_perfil_editar.php';
			$atletaData 	= Session::get('atleta');
			$treinadorData 	= Session::get('treinador');
			
			include(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblTreinadoresAtletas.class.php');
			$dbTblTreinAtleta = new dbTblTreinadoresAtletas();
			$dbTblTreinAtleta->set_IDTREINADOR($dbTblTreinAtleta->decryptVar($treinadorData['id']));
			$dbTblTreinAtleta->set_IDUSER($dbTblTreinAtleta->decryptVar(Session::get('id_user')));
			$dbTblTreinAtleta->set_IDATLETA($dbTblTreinAtleta->decryptVar($atletaData['id']));
			
			$objATPL = new TobjAutoTemplate();
			$objATPL->addObject('dbTblTreinAtleta',$dbTblTreinAtleta);
			$HTML = $objATPL->tpl_page($path.$file,true,false);
			
			unset($dbTblTreinAtleta);
			unset($objATPL);
			echo $HTML;
		}
		public static function perfil()
		{
			$path = SETPATH('ROOT','PATH_APP_UI_ATLETA');
			$file = 'user_perfil.php';
			$atletaData 	= Session::get('atleta');
			$treinadorData 	= Session::get('treinador');
		
			include(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblTreinadoresAtletas.class.php');
			$dbTblTreinAtleta = new dbTblTreinadoresAtletas();
			$dbTblTreinAtleta->set_IDTREINADOR($dbTblTreinAtleta->decryptVar($treinadorData['id']));
			$dbTblTreinAtleta->set_IDUSER($dbTblTreinAtleta->decryptVar(Session::get('id_user')));
			$dbTblTreinAtleta->set_IDATLETA($dbTblTreinAtleta->decryptVar($atletaData['id']));
			
			$objATPL = new TobjAutoTemplate();
			$objATPL->addObject('dbTblTreinAtleta',$dbTblTreinAtleta);
			$HTML = $objATPL->tpl_page($path.$file,true,false);
			
			unset($dbTblTreinAtleta);
			unset($objATPL);
			echo $HTML;
		}
		public static function listagem()
		{
			$path = SETPATH('ROOT','PATH_APP_UI_ATLETA');
			$file = 'user_listagem.php';
			$atletaData 	= Session::get('atleta');
			$treinadorData 	= Session::get('treinador');
			
			include(SETPATH('ROOT','PATH_APP_CORE_DB').'dbTblTreinadoresAtletas.class.php');
			$dbTblTreinAtleta = new dbTblTreinadoresAtletas();
			$dbTblTreinAtleta->set_IDTREINADOR($dbTblTreinAtleta->decryptVar($treinadorData['id']));
			$dbTblTreinAtleta->set_IDUSER($dbTblTreinAtleta->decryptVar(Session::get('id_user')));
			$dbTblTreinAtleta->set_IDATLETA($dbTblTreinAtleta->decryptVar($atletaData['id']));
		
			$jsonparams = base64_encode(json_encode(array(array(),array(),'atletas.nome','ASC')));
			
			$objATPL = new TobjAutoTemplate();
			$objATPL->setVar('{jsonparams}',$jsonparams);
			$objATPL->addObject('dbTblTreinAtleta',$dbTblTreinAtleta);
			$USERS_SECTION = $objATPL->tpl_page($path.'user_listagem_card.php',true,false,true);
			
			$objATPL->setVar('{USERS_SECTION}',$USERS_SECTION);
			$HTML = $objATPL->tpl_page($path.$file,true,false);
			
			unset($dbTblTreinAtleta);
			unset($objATPL);
			echo $HTML;
		}
	}
	
?>