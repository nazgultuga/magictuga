<?php
	function SETPATH($sTYPEPARENT,$sPATH)
	{
		$ARRAYPATH	= array('PATH_APP'									=> '',
							'PATH_APP_BOOT'								=> 'boot/',
							'PATH_APP_CORE'								=> 'core/',
							'PATH_APP_CORE_CONTROLLERS'					=> 'core/controllers/',
							'PATH_APP_CORE_DB'							=> 'core/db/',
							'PATH_APP_CORE_DBTOGGLE'					=> 'core/db_toggle/',
							'PATH_APP_CORE_SCRIPTS'						=> 'core/scripts/',
							'PATH_APP_CSS'								=> 'css/',
							'PATH_APP_IMG'								=> 'imgs/',
							'PATH_APP_JS'								=> 'js/',
							'PATH_APP_TMP'								=> 'tmp/',
							'PATH_APP_UI'								=> 'ui/',
							'PATH_APP_TEMPLATES'						=> 'templates/',
							'PATH_APP_UI_ATLETA'						=> 'ui/atleta/',
						);

		$PATH_ROOT	= $_SERVER['DOCUMENT_ROOT'].'/magic/';
		$PATH_URL	= '127.0.0.1/magic/';

		if ($sTYPEPARENT === 'ROOT')	// PARA FICHEIROS PHP
		{
			return $PATH_ROOT.$ARRAYPATH[$sPATH];
		}
		else
		if ($sTYPEPARENT === 'URL')		// PARA IMGS, CSS, JS
		{
			return $PATH_URL.$ARRAYPATH[$sPATH];
		}
	}
?>