<?php

	header('Content-type: text/html; charset=UTF-8');
	#$myfile = fopen("jsondata/9ED.json", "r");
	#$data = file_get_contents("jsondata/9ED.json");
	$data = file_get_contents("jsondata/SCG-x.json");
	$data = utf8_encode($data);
	#var_dump(json_decode($data));
	#$data = fgets($myfile);
	#fclose($myfile);

	$jsonData = json_decode($data);
	$jsonCards = $jsonData->cards;

	$SQL = "INSERT INTO `collections`(`id`,`name`,`date_year`,`total_cards`,`magic_set`) VALUES ";
	$SQL .= "(NULL,".
			$jsonData->name.",".
			date('Y',strtotime($jsonData->name)).",".
			count($jsonData->cards).",".
			$jsondata->code.")";
	

		function ms_escape_string($data)
		{
			$non_displayables = array(
			'/%0[0-8bcef]/', 			# url encoded 00-08, 11, 12, 14, 15
			'/%1[0-9a-f]/', 			# url encoded 16-31
			'/[\x00-\x08]/', 			# 00-08
			'/\x0b/', 					# 11
			'/\x0c/', 					# 12
			'/[\x0e-\x1f]/' 			# 14-31
			);

			foreach ($non_displayables as $regex)
			{
				$data = preg_replace($regex, '', $data);
			}

			return $data;
		}
		function escape($str)
		{
			$search  = array("\\","\0","\n","\r","\t","\x1a","'",'"');
			$replace = array("\\\\","\\0","\\n","\\r","\\t","\Z","\'",'\"');
			return str_replace($search,$replace,$str);
		}
		function StringProtect($AValue,$AEncode=true)
		{
			$AValue = trim($AValue);
			$AValue = ms_escape_string($AValue);
			$AValue = escape($AValue);
			if ($AEncode) $AValue = utf8_decode($AValue);
			return $AValue;
		}
		function protectVar($AValue)
		{
			return StringProtect($AValue);
		}

	function getPTName($object)
	{
		for($i=0, $ilen=count($object); $i<$ilen; ++$i)
		{
			if ($object[$i]->language === "Portuguese (Brazil)")
			{
				return $object[$i]->name;
				break;
			}
		}
		return '';
	}

	$id_collection = '4';
	$data_alterado = date('Y-m-d H:i:s');
	$arrSQL = array();
	$iniSQL = "INSERT INTO `cards`(`id`,`id_collection`,`magic_set`,`type`,`multiverseid`,`name_eng`,`name_pt`,`rarity`,`artist`,`text_eng`,`text_pt`,`number`,`manaCost`,`colors`,`power`,`toughness`,`flavor`,`stock`,`price`,`data_alterado`) VALUES ";
	foreach ($jsonCards as $key => &$value)
	{
		$name_pt 		= protectVar(getPTName($value->foreignNames));
		$magic_set 		= protectVar($jsonData->code);
		$type 			= protectVar($value->type);
		$multiverseid 	= protectVar($value->multiverseid);
		$name 			= protectVar($value->name);
		$rarity 		= protectVar($value->rarity);
		$artist 		= protectVar($value->artist);
		$text 			= protectVar($value->text);
		$number 		= protectVar($value->number);
		$colors 		= ($value->colors) ? protectVar(implode(';',$value->colors)) : '';
		$manaCost 		= protectVar($value->manaCost);
		$power 			= protectVar($value->power);
		$toughness 		= protectVar($value->toughness);
		$flavor 		= protectVar($value->flavor);

		#UPDATE PT-PT names
		#$arrSQL[] = "UPDATE `cards` SET `name_pt`='$name_pt' WHERE multiverseid='$multiverseid';";

		$arrSQL[] = "(NULL,$id_collection,
								'$magic_set',
								'$type',
								'$multiverseid',
								'$name',
								'$name_pt',
								'$rarity',
								'$artist',
								'$text',
								NULL,
								'$number',
								'$manaCost',
								'$colors',
								'$power',
								'$toughness',
								'$flavor',
								0,
								0.0,
								'$data_alterado')";

	}
	echo $iniSQL.implode(",\n", $arrSQL);
	#echo implode("\n", $arrSQL);

?>