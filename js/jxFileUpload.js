
	var upload = 
	{
		name: 'upload',
		Parent: jx,

		iFiles: 0,
		files: false,
		BinaryData: false,
		eFileInput: false,
		eDragDrop: false,
		iMaxSize: 5242880, // 5MB
		UrlTarget: '',
		UrlParams: '',
		UrlTempImage: '', 		//Temporary link of the uploaded image.
		isJson: false,			//the Result from  "UrlTarget" is "JSON"?.
		eImagePreview: false, 	//object where image will be shown.
		InputNameToPost: '', 	//input name, to send when POSTing information.

		onSaveSucess: false,
		onLoadProgress: false,
		onUploadCompleted: false,
		onFileChange: false, 	//When [input type="file"] changes, a function is trigged.
	
		setUrlTarget: function(value){ this.UrlTarget = value; return this; },
		setMaxSize: function(value)  { this.iMaxSize  = value; return this; },
		setInputFile: function(AsName)
		{
			var Self = this;
			this.eFileInput = this.Parent.get(AsName,0);
			if (this.onFileChange == false)
			{
				this.onFileChange = function(event)
				{
					jx.upload.upload();
				};
			}

			this.Parent.set(this.eFileInput).addEventHandler("change", function(event)
			{
				Self.files = event.target.files;
				Self.execCallback(Self.onFileChange,event);
			},false);
			return this;
		},
		setImagePreview: function()
		{
			if (this.eImagePreview)
			{
				this.Parent.set(this.eImagePreview,0).setAtt('src',this.Parent.url.get().host+"tmp/"+this.UrlTempImage);
			}
		},
		setImagePath: function()
		{
			if (this.InputNameToPost !== '')
			{
				var eEdtFile = this.eFileInput.form.elements[this.InputNameToPost];
				if (eEdtFile == undefined)
				{
					this.eFileInput.form.insertAdjacentHTML('beforeend','<input type="hidden" name="'+this.InputNameToPost+'" value="'+this.UrlTempImage+'" >');
				}
				else
				{
					eEdtFile.setAttribute('value',this.UrlTempImage);
				}
			}
		},
		addParam: function(option,value)
		{
			this.UrlParams += '&'+option+'='+value;
			return this;
		},
		generateBoundary: function()
		{
			return "AJAX-----------------------" + (new Date).getTime();
		},
		execCallback: function(fCallback,ParamResult)
		{
			if (this.Parent.isFunction(fCallback))
				fCallback(ParamResult);
		},
		encodeURI3986: function(str)
		{
			return encodeURIComponent(str).
				// Note that although RFC3986 reserves "!", RFC5987 does not,
				// so we do not need to escape it
				replace(/['()]/g, escape). // i.e., %27 %28 %29
				replace(/\*/g, '%2A').
				// The following are not required for percent-encoding per RFC5987, 
				//  so we can allow for a little better readability over the wire: |`^
				replace(/%(?:7C|60|5E)/g, unescape);
		},
		setDragDrop: function(AObject, fCallback, SendDataFile)
		{
			this.Parent.set(AObject,0);
			var Self = this; 
			function drop(event)
			{
				event.preventDefault();
				var Result = event;
				
				if (!SendDataFile)
				{
					Self.files 	= event.dataTransfer.files;
					Result 		= event.dataTransfer.files;
				}
				
				Self.execCallback(fCallback,Result);
			}
			function dragstart(e) {}
			function dragenter(e)
			{
				e.stopPropagation();
				e.preventDefault();
			}
			function dragover(e)
			{
				e.stopPropagation();
				e.preventDefault();
			}
			function dragleave(e) {}
			function drag(e) {}
			function dragend(e) {}
			
			this.Parent.addEventHandler("dragstart", dragstart, true);
			this.Parent.addEventHandler("dragenter", dragenter, false);
			this.Parent.addEventHandler("dragover", dragover, true);
			this.Parent.addEventHandler("dragleave", dragleave, false);
			this.Parent.addEventHandler("drag", drag, false);
			this.Parent.addEventHandler("drop", drop, false);
			this.Parent.addEventHandler("dragend", dragend, false);
			//this.eDragDrop.addEventListener("dragend", dragend, false);
		},
		upload: function()
		{
			this.iFiles = 0;
			this.BinaryData = false;
			if (window.FileReader)
			{
				for(var i=0, ccI=this.files.length; i<ccI; ++i)
				{
					if (this.files[i].size <= this.iMaxSize)
					{
						this.readFile(this.files[i]);
					}
					else
					{
						console.log('The file "'+this.files[i].name+'" is too big! '+"\n"+
									'Size: '+this.bytesToSize(this.files[i].size)+"\n"+
									'Max: '+this.bytesToSize(this.iMaxSize));
					}
				}
			}
			else
			{
				console.log('Update your browser!');
			}
		},
		readFile: function(File,CallbackProgress,CallbackDone)
		{
			var objReader = new FileReader;
			var Self 	  = this;
			
			objReader.onload = function (e)
			{
				console.info("[Status] Reading successfully completed. {" + File.name + "}");
				Self.BinaryData[Self.iFiles-1] = e.target.result;
			};
			objReader.onerror = function(event)
			{
				var errorMsg = '';
				e = event || window.event;
				switch (e.target.error.code)
				{
					case e.target.error.NOT_FOUND_ERR: 		errorMsg = "File not found!"; break;
					case e.target.error.NOT_READABLE_ERR: 	errorMsg = "File not readable!"; break;
					case e.target.error.ABORT_ERR: 			errorMsg = "Read operation aborted!"; break;
					case e.target.error.SECURITY_ERR: 		errorMsg = "File locked!"; break;
					case e.target.error.ENCODING_ERR: 		errorMsg = "Encoding file. data:// URL"; break;
					default: 								errorMsg = "Read error!"; break;
				}
				console.error("[ERROR] "+errorMsg+"{" + File.name + "}");
			};
			objReader.onabort = function(e)
			{
				console.info("Read operation ABORTED.");
			};
			objReader.onloadstart = function(e)
			{
				console.info("[Status] Reading STARTED. {'" + File.name + "'}")
			};
			
			if (this.Parent.isFunction(this.onLoadProgress))
				objReader.onprogress = this.onLoadProgress(event);
			else
				objReader.onprogress = function(e)
				{
					if (e.lengthComputable === true)
					{
						var loaded = (e.loaded / e.total);
						if (loaded < 1)
						{
							//var percent = Math.round((event.loaded * 100) / event.total);
							//eObjProg.style.height = percent+'%';
						}
					}
					var percent = Math.round((e.loaded * 100) / e.total);
					
					console.info("[Status] "+percent+"% done.");
					console.info("[Status] Reading CHUNK. {"+e.loaded+"/"+e.total+"} {"+File.name+"}");
					console.info("[Status] Reading DATA. {'"+File.name+"'}");
				};
			
			objReader.onloadend = function(e)
			{
				console.info("[Status] Reading COMPLETED. {'" + File.name + "'}");
				Self.sendFile(File, Self.BinaryData[Self.iFiles-1]);
			};
			
			this.iFiles++;
			this.BinaryData[this.iFiles-1] = '';
			objReader.readAsBinaryString(File);
		},
		sendFile: function(File,FileData)
		{
			Self = this;
			var ObjAjax = this.Parent.newXMLHTTP();
			ObjAjax.onreadystatechange = function()
			{
				if ((ObjAjax.readyState == 4) && (ObjAjax.status == 200))
				{
					var sResult = Self.Parent.trim(ObjAjax.responseText);
					if (Self.isJson)
					{
						var jsonData = Self.Parent.ParseJSON(sResult);
						if (jsonData.result)
						{
							Self.UrlTempImage = jsonData.filename;
							Self.setImagePreview();
							Self.setImagePath();

							if (Self.onSaveSucess) 
								Self.execCallback(Self.onSaveSucess,sResult);
							else
							{
								console.error("[Upload Sucess] File: '"+File.name+"'.");
							}
						}
						else
						{
							if (Self.onSaveError)
								Self.execCallback(Self.onSaveError,sResult);
							else
							{
								console.error("[Upload Error] File: '"+File.name+"'.");
							}
						}
					}
					else
					{
						Self.execCallback(Self.onUploadCompleted,sResult);
					}
				}
			}
			
			if (FileData && jx.Parent.isString(FileData))
			{
				if (ObjAjax.sendAsBinary != null)
				{
					ObjAjax.open("POST", this.UrlTarget + "?up=true"+this.UrlParams, true);
					//var r = "xxxxxxxxx";
					var boundary = this.generateBoundary();
					var bin = "--" + boundary + "\r\n";
						bin += "Content-Disposition: form-data; name='upload'; filename='" + this.encodeURI3986(File.name) + "'\r\n";
						bin += "Content-Type: application/octet-stream\r\n\r\n";
						bin += FileData + "\r\n";
						bin += "--" + boundary + "--";
					ObjAjax.setRequestHeader("content-type", "multipart/form-data; boundary=" + boundary);
					ObjAjax.sendAsBinary(bin);
				}
				else
				{
					ObjAjax.open("POST", this.UrlTarget+"?up=true&base64=true"+this.UrlParams, true);
					ObjAjax.setRequestHeader("UP-FILENAME", this.encodeURI3986(File.name));
					ObjAjax.setRequestHeader("UP-SIZE", File.size);
					ObjAjax.setRequestHeader("UP-TYPE", File.type);
					ObjAjax.send(window.btoa(File));
				}
			}
			else
			{
				ObjAjax.open("POST", this.UrlTarget+"?up=true"+this.UrlParams, true);
				ObjAjax.setRequestHeader("UP-FILENAME", this.encodeURI3986(File.name));
				ObjAjax.setRequestHeader("UP-SIZE", File.size);
				ObjAjax.setRequestHeader("UP-TYPE", File.type);
				ObjAjax.send(File);
			}
		},
		set: function(oData)
		{
			if (oData.hasOwnProperty('target'))
				this.setUrlTarget(oData.target);
			if (oData.hasOwnProperty('maxsize'))
				this.setMaxSize(oData.maxsize);
			if (oData.hasOwnProperty('onFileChange'))
				this.onFileChange = oData.onFileChange;
			if (oData.hasOwnProperty('inputfile'))
				this.setInputFile(oData.inputfile);
			if (oData.hasOwnProperty('onSaveSucess'))
				this.onSaveSucess = oData.onSaveSucess;
			if (oData.hasOwnProperty('onSaveError'))
				this.onSaveError = oData.onSaveError;
			if (oData.hasOwnProperty('onLoadProgress'))
				this.onLoadProgress = oData.onLoadProgress;
			if (oData.hasOwnProperty('isJson'))
				this.isJson = oData.isJson;
			if (oData.hasOwnProperty('ImagePreview'))
				this.eImagePreview = this.Parent.get(oData.ImagePreview);
			if (oData.hasOwnProperty('inputToPost'))
				this.InputNameToPost = oData.inputToPost;

			return this;
		}
	}

	jx.extend(upload);