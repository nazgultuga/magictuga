	var forms =
	{
		name: 'forms',
		Parent: jx,
		form: false,
		elements: false,
		elemslength: 0,
		formname: '',
		urlAjax: '/magic/core/db_toggle/ajax-toggle-record.php',

		onNotValid : false,
		onBunk : false,
		onDebunk : false,
		onRecordSavedError : false,
		onRecordSaved : false,
		onRemoveError : false,
		onValidateFields : false,
		onFieldEmpty : false,

		set: function(value)
		{
			this.form = (this.Parent.isObject(value)) ? value : document.forms[value];
			this.elements = this.form.elements;
			this.elemslength = this.elements.length;
			this.formname = this.form.name;
			return this;
		},
		get: function()
		{
			return this.form;
		},
		setUrlAjax: function(urlink)
		{
			this.urlAjax = urlink;
			return this;
		},
		getSelectedRadio: function(fields)
		{
			if (fields[0] != undefined)
			{
				for(var i=0, ilen=fields.length; i<ilen; ++i)
				{
					if ( (fields[i].getAttribute("checked") == null) ||
						 (fields[i].getAttribute("selected") == null) )
					{
						return fields[i];
					}
				}
			}
			return fields;
		},
		clean: function(formvalue,AarrResetField,AarrResetTo)
		{
			if (!this.Parent.isNullUndef(formvalue))
				this.set(formvalue);

			var sRadio 	= '';
			var eObj 	= false;
			var ccJ 	= (AarrResetField != undefined) ? AarrResetField.length : 0;
			var bChange = false;
			var sResetValue = '';
			
			for (var i=0; i<this.elemslength; i++)
			{
				eObj 		= this.elements[i];
				bChange 	= false;
				sResetValue = '';
				
				/* PROCURA CAMPO PARA MODIFICAR */
				for(var j=0; j<ccJ; j++)
				{
					if (eObj.name === AarrResetField[j])
					{
						bChange 	= true;
						sResetValue = AarrResetTo[j];
						break;
					}
				}
				
				if (eObj.getAttribute("type") === "radio")
				{
					var radios = this.elements[eObj.name];
						sValue = false;
					for (var j=0, jlen=radios.length; j<jlen; ++j)
					{
						radios[j].checked = false;
					}
				}
				else
				if ((eObj.tagName === "INPUT") || (eObj.tagName === "TEXTAREA"))
				{
					eObj.value = (bChange !== false) ? sResetValue : '';
				}
				else
				if (eObj.tagName === "SELECT")
					eObj.selectedIndex = (bChange !== false) ? sResetValue : 0;
			}
			return this;
		},
		serialize: function(formvalue)
		{
			if (!this.Parent.isNullUndef(formvalue))
				this.set(formvalue);

			var jsonRequired 	= {};
			var arrJson 		= {};
			
			var vField 		= null;
			var sName 		= '';
			var sValue 		= '';
			var sTitle 		= '';
			var bRequired 	= false;
			var addField 	= false;
			
			for (var i=0; i<this.elemslength; ++i)
			{
				vField = this.elements[i];
				
				if ((vField.tagName === "INPUT") || (vField.tagName === "TEXTAREA") || (vField.tagName === "SELECT"))
				{
					sName 		= vField.name;
					bRequired 	= (vField.getAttribute("required") != null);
					sValue 		= '';
					sTitle 		= '';
					addField 	= true;
					
					if (vField.tagName === "INPUT")
					{
						switch(vField.type)
						{
							case 'radio': 
								{
									var radios = this.elements[sName];
									//var eLabel = null;
										sValue = false;
									for (var j=0, jlen=radios.length; j<jlen; ++j)
									{
										if (radios[j].checked === true)
										{
											sValue = radios[j].value;
											sTitle = radios[j].getAttribute('title');
											//eLabel = this.Parent.get({tag:'LABEL',att:'for',name:radios[j].getAttribute('id')});
											break;
										}
									}
								} break;
							case 'checkbox':
								{
									if (vField.checked == true)
									{
										sValue = vField.value;
									}
								} break;
							case 'number':
							case 'text':
							case 'hidden':
							case 'date':
							case 'tel':
							case 'email':
							case 'time':
								{
									sValue = (vField.getAttribute("data-setpost") === "false") ? '' : this.Parent.trim(vField.value);
								} break;
							default: 
								{
									addField = false;
								} break;
						}
					}
					else
					if (vField.tagName === "TEXTAREA")
					{
						sValue = this.Parent.trim(vField.value);
					}
					else
					if (vField.tagName === "SELECT")
					{
						if (vField.selectedIndex <= 0)
						{
							sValue = vField.selectedIndex;
						}
						else
						{
							sValue = vField.options[vField.selectedIndex].value;
							sTitle = vField.options[vField.selectedIndex].text;
						}
					}
					
					if ((sName !== '') && (addField))
					{
						jsonRequired[sName] = bRequired;
						arrJson[sName] = sValue;
						if (sTitle !== '')
							arrJson[sName+'-title'] = sTitle;
					}
				}
			}
			
			return {'required':JSON.stringify(jsonRequired), 'fields':JSON.stringify(arrJson)};
		},
		submit: function(formvalue,AOperation,fCallback,AsRedirectURL,AsExtraparams)
		{
			AOperatio     = ((this.Parent.isNullUndef(AOperation)) || (AOperation === '')) ? 'update' : AOperation;
			fCallback 	  = (this.Parent.isNullUndef(fCallback)) ? false : fCallback;
			AsExtraparams = (this.Parent.isNullUndef(AsExtraparams)) ? '' : AsExtraparams;
			AsRedirectURL = (this.Parent.isNullUndef(AsRedirectURL)) ? false : AsRedirectURL;

			if (fCallback == false)
			{
				var Self = this;
				fCallback = function(sResult)
				{
					console.log(sResult);
					Self.formname = '';
					if (sResult !== '')
					{
						var jsonData = Self.Parent.ParseJSON(sResult);
						if (jsonData)
						{
							if (jsonData.result)
							{
								if (Self.Parent.isFunction(Self.onRecordSaved))
									Self.onRecordSaved(jsonData,AsRedirectURL);
								else
								{
									alert('Registo guardado com sucesso!');
									if (Self.Parent.isString(AsRedirectURL))
										window.location.replace(AsRedirectURL);
								}
								
								//Self.onRecordSaved = false;
								return true;
							}
							else
							{
								if (Self.Parent.isFunction(Self.onRecordSavedError))
									Self.onRecordSavedError(jsonData);
								else
									alert("Ocorreu um erro. \n\t Porfavor tente novamente.");
								
								//Self.onRecordSavedError = false;
								return false;
							}
						}
					}
					return false;
				};
			}

			var jsonData 	 = this.serialize(formvalue);
			var jsonFields 	 = this.Parent.ParseJSON(jsonData.fields);
			var jsonRequired = this.Parent.ParseJSON(jsonData.required);

			var bValid = true;
			var eField = false;
			var elements = false;
			var sFTitle = '';
			var tagName = '';
			
			if (this.Parent.isFunction(this.onValidateFields))
			{
				bValid = this.onValidateFields(this.form,jsonFields,jsonRequired);
				if (!bValid)
					this.formname = '';
			}
			else
			{
				for (var key in jsonRequired)
				{
					elements = this.form.elements[key];
					tagName  = elements.tagName;
					if (tagName === 'SELECT')
					{
						eField = this.getSelectedRadio(elements);
						if (eField.value >= 0)
							break;
					}

					// Campo obrigatório e vazio.
					if ( (jsonRequired[key] === true) &&
						 (  ((tagName === 'SELECT') && (jsonFields[key] == 0)) ||
							((jsonFields[key] === '') || (jsonFields[key] === 'false'))
						 )
						)
					{
						{
							bValid = false;
							eField = (tagName === 'SELECT') ? elements : this.getSelectedRadio(elements);
							eField.focus();
							
							if (this.Parent.isFunction(this.onNotValid))
								this.onNotValid(eField);
							else
							{
								sFTitle = eField.getAttribute("placeholder");
								if (sFTitle == null)
								{
									sFTitle = eField.getAttribute('id');
									if (sFTitle != null)
									{
										var eLabel = this.Parent.get({tag:'LABEL',att:'for',name:sFTitle});
										if (eLabel)
											sFTitle = this.Parent.html();
									}
									else
										sFTitle = eField.name;
								}
								
								if (this.Parent.isFunction(this.onFieldEmpty))
								{
									bValid = this.onFieldEmpty(eField,sFTitle);
									if (!bValid)
										this.formname = '';
								}
								else
								{
									if (!this.Parent.get({tag:'msg',att:'id',name:eField.name}))
									{
										var style = 'color:#FFF; font-size:0.7em; padding:0.5em; background:#ff6666; border-radius:4px; display:block; margin-top:4px; text-align:center;';
										eField.parentNode.insertAdjacentHTML('beforeend','<msg id="'+eField.name+'" style="'+style+'">*Campo de preenchimento obrigatório!</msg>');
									}
									bValid = false;
								}
								//alert("O campo '"+sFTitle+"' encontra-se vazio.");
							}
							break;
						}
					}
					else
					{
						eField = (tagName === 'SELECT') ? elements : this.getSelectedRadio(elements);
						var msg = this.Parent.get({tag:'msg',att:'id',name:eField.name});
						if (msg)
						{
							jx.set(msg).remove();
						}
					}
				}
			}
			
			if (bValid)
			{
				/*
				if (!this.Parent.isNullUndef(formvalue))
				{
					if (this.formname === '')
					{
						this.set(formvalue);
					}
					else
					if (formvalue === this.formname)
					{
						console.log('Já em uso!');
						return false;
					}
				}
				*/
				//this.onNotValid 	= false;
				//this.onFieldEmpty = false;
				
				var objParams = {
					form: this.formname,
					action: AOperation,
					data: jsonData.fields,
				};
				this.Ajax(objParams,AsExtraparams,fCallback);
				return true;
			}
			else
				return false;
		},
		obj2params: function(object)
		{
			var result = '';
			for(var key in object)
			{ 
				result += '&'+key+'='+object[key];
			}
			return result;
		},
		Ajax: function(AobjPars,AsExtraParams,fCallback)
		{
			var url 	= this.urlAjax;
			var params 	= '';
			for(var key in AobjPars)
			{ 
				params += '&'+key+'='+AobjPars[key];
			}
			if (AsExtraParams[0] !== '&')
				AsExtraParams = '&'+AsExtraParams;
			params += AsExtraParams;
			
			this.Parent.ajax({type:'POST', url:url, data:params, done:fCallback});
		},
	};
	jx.extend(forms);

