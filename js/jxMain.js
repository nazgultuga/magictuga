
	function elem()
	{
		this.eElement 	 = false;
		this.length 	 = 0;
		this.SCROLL_POSY = 0;
		this.SCROLL_POSX = 0;
		this.hideElement = false;
		
		this.searchObj = function(aData,aIndex)
		{
			aIndex = (aIndex != undefined) ? aIndex : true;
			aData  = (aData != undefined) ? aData : false;

			switch(typeof(aData))
			{
				case 'string':
					{
						var sCode = aData[0];
						//var sLen  = aData.length;
						var sName = aData.substr(1);
						switch(sCode)
						{
							case '#': // ID
								{
									this.eElement = this.getElementById(sName);
								} break;
							case '.': // ClassName
								{
									this.eElement = this.getElementsByClassName(sName,aIndex);
								} break;
							case '*': // TagName
								{
									this.eElement = this.getElementsByTagName(sName,aIndex);
								} break;
							default: // Name
								{
									this.eElement = this.getElementsByName(aData,aIndex);
								} break;
						}
					} break;
				case 'object':
					{
						if (aData.hasOwnProperty('eElement'))
						{
							this.eElement = aData.eElement;
						}
						else
						if (aData.hasOwnProperty('tag') && aData.hasOwnProperty('att'))
						{
							this.eElement = this.getElementsByTagNameData(aData.tag,aData.att,aData.name);
							this.eElement = ((this.eElement) && (this.isInteger(aIndex) && (aIndex>=0))) ? this.eElement[aIndex] : this.eElement;
						}
						else
						if (aData.hasOwnProperty('xform'))
						{
							var eForm 	  = (this.isString(aData.xform)) ? document.forms[aData.xform] : aData.xform;
							var eElem 	  = (this.isString(aData.input))? eForm.elements[aData.input] : aData.input;
							this.eElement = ((!this.isNullUndef(this.eElement)) && (this.isInteger(aIndex) && (aIndex>=0))) ? eElem[aIndex] : eElem;
						}
						else
						{
							this.eElement = aData;
						}
					} break;
				case 'boolean':
					{
						this.eElement = false;
					} break;
				default:
					{
						console.log(typeof(aData));
					} break;
			}
			this.length = 0;
			if (!this.isNullUndef(this.eElement))
				if (this.eElement.hasOwnProperty('length'))
				{
					this.length = this.eElement.length;
					if ((this.eElement.length == 1) && (aIndex==true))
					{
						this.eElement = this.eElement[0];
						this.length = 0;
					}
					else
					if (this.eElement.length == 0)
					{
						this.eElement = false;
						this.length = 0;
					}
				}
			return this;
		};
		this.get = function(aData,aIndex)
		{
			return this.searchObj(aData,aIndex).eElement;
		};
		this.set = function(aData,aIndex)
		{
			return this.searchObj(aData,aIndex);
		};
		this.getElement = function()
		{
			return this.eElement;
		};

		this.foreach = function(AarrObj,fCallback,Self)
		{
			if (this.isFunction(fCallback))
			{
				for (var i=0, ilen=AarrObj.length; i<ilen; ++i)
				{
					fCallback(AarrObj[i],i,this);
				}
			}
			return this;
		};
		this.foreachReverse = function(AarrObj,fCallback,Self)
		{
			if (this.isFunction(fCallback))
			{
				for (var i=AarrObj.length-1; i>=0; --i)
				{
					fCallback(AarrObj[i],i,this);
				}
			}
			return this;
		};
		this.log = function(parameter)
		{
			console.log(parameter);
			return this;
		}

		/* OBJECT FIND-SEARCH */
		this.getElementById = function(aName,aIndex)
		{
			return document.getElementById(aName);
		};
		this.getElementsByClassName = function(aName,aIndex)
		{
			return (this.isInteger(aIndex) && (aIndex>=0)) ? 
				document.getElementsByClassName(aName)[aIndex] :
				document.getElementsByClassName(aName);
		};
		this.getElementsByTagName = function(aName,aIndex)
		{
			return (this.isInteger(aIndex) && (aIndex>=0)) ? 
				document.getElementsByTagName(aName)[aIndex] :
				document.getElementsByTagName(aName);
		};
		this.getElementsByName = function(aName,aIndex)
		{
			return (this.isInteger(aIndex) && (aIndex>=0)) ? 
				document.getElementsByName(aName)[aIndex] :
				document.getElementsByName(aName);
		};
		this.getElementsByTagNameData = function(aTagName, aDataName, aName)
		{
			var arrObjs = this.getElementsByTagName(aTagName);
			var Result  = [];
			var k 		= -1;
			for (var i=0, ilen=arrObjs.length; i<ilen; ++i)
			{
				if (arrObjs[i].getAttribute(aDataName) === aName)
				{
					Result[++k] = arrObjs[i];
				}
			}
			return (Result[0] != undefined) ? Result : undefined;
		};
		this.getElementForm = function(object)
		{
			object = (object != undefined) ? object.parentNode : this.eElement.parentNode;
			while (object != undefined)
			{
				if (object.tagName !== 'FORM')
					object = object.parentNode;
				else
					return object.getAttribute('name');
			}
			return '';
		};
		this.getFormByName = function(name)
		{
			return document.forms[name];
		};

		this.equalTo = function(object)
		{
			return (this.eElement == object);
		};


		/* WINDOW MANIPULATION */
		/* BROWSER MANIPULATION*/
		this.getPageScrollPos = function()
		{
			this.SCROLL_POSY = document.body.scrollTop;
			this.SCROLL_POSX = document.body.scrollLeft;
		};
		this.setScrollPos = function(posX,posY)
		{
			posX = (this.isInteger(posX)) ? posX : this.SCROLL_POSX;
			posY = (this.isInteger(posY)) ? posY : this.SCROLL_POSY;
			window.scrollTo(posX,posY); 
		};
		// http://stackoverflow.com/questions/4976936/
		// http://stackoverflow.com/questions/3333329/javascript-get-browser-height
		this.getWindowDimensions = function()
		{
			var iWidth = 0,
				iHeight = 0;
			if (typeof(window.innerWidth)== 'number')
			{ //Non-IE
				iWidth = window.innerWidth;
				iHeight = window.innerHeight;
			}
			else
			if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight))
			{ //IE 6+ in 'standards compliant mode'
				iWidth = document.documentElement.clientWidth;
				iHeight = document.documentElement.clientHeight;
			}
			else
			if (document.body && (document.body.clientWidth || document.body.clientHeight))
			{ //IE 4 compatible
				iWidth = document.body.clientWidth;
				iHeight = document.body.clientHeight;
			}
			return {height:iHeight, width:iWidth};
		};
		/* EVENTs HANDLERs MANIPULATION */
		this.addEventHandler = function(eventType,ACallback,useCapture)
		{
			useCapture = (!this.isNullUndef(useCapture)) ? useCapture : false;

			if (this.eElement.addEventListener)
				this.eElement.addEventListener(eventType,ACallback,useCapture);
			else
			if (this.eElement.attachEvent)
				this.eElement.attachEvent('on'+eventType,ACallback); 
		};
		/* OBJECT MANIPULATION */
		this.element =
		{
			Parent: this,
			hasNode: function(element,child)
			{
				if (!this.Parent.isNullUndef(element.parentNode))
				{
					var children = element.parentNode.children;
					for(var i=0, ilen=children.length; i<ilen; ++i)
					{
						if (children[i] == child)
							return i;
					}
				}
				return -1;
			},
			indexOfNode: function(element)
			{
				if (!this.Parent.isNullUndef(element.parentNode))
				{
					var children = element.parentNode.children;
					for(var i=children.length-1; i>=0; --i)
					{
						if (children[i] == element)
							return i;
					}
				}
				return -1;
			},
			clean: function(element)
			{
				while(element.firstChild)
				{
					element.removeChild(element.firstChild);
				};
			},
			// http://stackoverflow.com/questions/1910794#18418270
			replace: function(element,AnewHTML,APosition)
			{
				this.clean(element);
				this.append(element,AnewHTML,APosition);
			},
			replaceWith: function(element,AnewHTML)
			{
				var auxElem = false;
				var index = this.indexOfNode(element);
				if (index!=-1)
				{
					auxElem = element.parentNode.children[index];
					auxElem.insertAdjacentHTML('afterend',AnewHTML);
					element.parentNode.removeChild(auxElem);
				}
			},
			append: function(element,AnewHTML,APosition)
			{
				APosition = (this.Parent.isString(APosition)) ? APosition : 'afterbegin';
				element.insertAdjacentHTML(APosition,AnewHTML);
			},
			remove: function(element)
			{
				if (element.hasOwnProperty('remove'))
				{
					element.remove();
				}
				else
				if (element.parentNode != undefined)
				{
					element.parentNode.removeChild(element);
				}
				else
				if (element.hasOwnProperty('outerHTML')) 
				{
					element.outerHTML = "";
				}
				else
				{
					console.log(element);
				}
			},
		};
		this.clean = function()
		{
			if (this.length>0)
			{
				this.foreach(this.eElement,function(object,index,Self){
					Self.element.clean(object);
				});
			}
			else
			{
				this.element.clean(this.eElement);
			}
			return this;
		};
		this.replace = function(AnewHTML,APosition)
		{
			if (this.length>0)
			{
				this.foreach(this.eElement,function(object,index,Self){
					Self.element.replace(object,AnewHTML,APosition);
				});
			}
			else
			{
				this.element.replace(this.eElement,AnewHTML,APosition);
			}
			return this;
		};
		this.replaceWith = function(AnewHTML)
		{			
			if (this.length>0)
			{
				this.foreachReverse(this.eElement,function(object,index,Self){
					Self.element.replaceWith(object,AnewHTML);
				});
			}
			else
			{
				this.element.replaceWith(this.eElement,AnewHTML);
			}
			return this;
		};
		this.append = function(AnewHTML,APosition)
		{
			if (this.length>0)
			{
				this.foreach(this.eElement,function(object,index,Self){
					Self.element.append(object,AnewHTML,APosition);
				});
			}
			else
			{
				this.element.append(this.eElement,AnewHTML,APosition);
			}
			return this;
		};
		this.remove = function()
		{
			if (this.length>0)
			{
				this.foreach(this.eElement,function(object,index,Self){
					Self.element.remove(object);
				});
			}
			else
			{
				this.element.remove(this.eElement);
			}
			return this;
		};
		this.html = function(value,replace)
		{
			value   = (this.isNullUndef(value)) ? false : value;
			replace = (this.isNullUndef(replace)) ? false : replace;
			if (value == false)
			{
				return this.eElement.innerHTML;
			}
			else
			{
				if ((value === '') && (!replace))
					this.clean();
				else
				{
					if (!replace)
						return this.replace(value);
					else
						return this.replaceWith(value);
				}
			}
		};
		this.ParseHTMLToDOM = function(html)
		{
			var tmp = document.implementation.createHTMLDocument();
				tmp.body.insertAdjacentHTML('afterbegin',html);
			return tmp.body.children[0];
		};

		/* ATTRIBUTES MANIPULATION */
		this.attributes = 
		{
			Parent: this,
			set: function(element,att,value)
			{
				element.setAttribute(att,value);
			},
			get: function(element,att)
			{
				return element.getAttribute(att);
			},
			add: function(element,att,value)
			{
				var sAttValue = element.getAttribute(att);
					sAttValue = (sAttValue == null) ? '' : sAttValue+' ';
				element.setAttribute(att,sAttValue+value);
			},
			remove: function(element,att)
			{
				element.removeAttribute(att);
			},
		};
		this.getAtt = function(att)
		{
			return this.attributes.get(this.eElement,att);
		};
		this.setAtt = function(att,value)
		{
			if (this.isNullUndef(value))
				return this.removeAtt(att);
			if (this.length>0)
			{
				this.foreach(this.eElement,function(object,index,Self){
					Self.attributes.set(object,att,value);
				});
			}
			else
			{
				this.attributes.set(this.eElement,att,value);
			}
			return this;
		};
		this.addAtt = function(att,value)
		{
			if (this.length>0)
			{
				this.foreach(this.eElement,function(object,index,Self){
					Self.attributes.add(object,att,value);
				});
			}
			else
			{
				this.attributes.add(this.eElement,att,value);
			}
			return this;
		};
		this.removeAtt = function(att)
		{
			if (this.length>0)
			{
				this.foreach(this.eElement,function(object,index,Self){
					Self.attributes.remove(object,att);
				});
			}
			else
			{
				this.attributes.remove(this.eElement,att);
			}
			return this;
		};

		/* STYLE OBJECT MANIPULATION */
		this.style =
		{
			Parent: this,
			stylesheet: false,
			cssRulesSize: 0,

			// http://israelidanny.github.io/veinjs/
			// http://davidwalsh.name/add-rules-stylesheets
			// https://github.com/israelidanny/veinjs/blob/master/vein.js
			createStyleSheet: function()
			{
				var style = document.createElement("style");
					style.id   = "jxStyle";
					style.type = "text/css";
					// Apparently some version of Safari needs the following line? I dunno.
					style.appendChild(document.createTextNode(''));
				document.head.appendChild(style);
			},
			getStyleSheet: function()
			{
				this.stylesheet = false;
				this.cssRulesSize = 0;
				var sheets = document.styleSheets;
				for (var i=sheets.length-1; i>=0; --i)
				{
					if (sheets[i].ownerNode.id === "jxStyle")
					{
						this.stylesheet = sheets[i];
						this.cssRulesSize = (this.stylesheet.cssRules == null) ? 0 : this.stylesheet.cssRules.length;
						return true;
					}
				}
			},
			getSheet: function()
			{
				this.getStyleSheet();
				if (this.Parent.isNullUndef(this.stylesheet))
				{
					this.createStyleSheet();
				}
				this.getStyleSheet();
			},
			searchSelector: function(value)
			{
				for(var i=0; i<this.cssRulesSize; ++i)
				{
					if (this.stylesheet.cssRules[i].selectorText === value)
					{
						return i;
					}
				}
				return -1;
			},
			addCSSRule: function(selector, rules, index)
			{
				this.getSheet();
				if (!this.Parent.isInteger(index))
				{
					index = this.cssRulesSize;
				}

				if (this.Parent.isNullUndef(rules))
				{
					this.deleteRule(selector);
				}
				else
				if (this.cssRulesSize>0)
				{
					if (this.cssRulesSize>=index)
					{
						var auxIndex = this.searchSelector(selector);
						if ((auxIndex != index) && (auxIndex != -1))
							index = auxIndex;
						else
							this.addRule(selector,rules,index);

						var rule = this.getRule(index);
						var cssJs = '';
						for(var key in rules)
						{
							cssJs = this.camelCase(key);
							if (cssJs)
								rule.style.setProperty(this.camelCase(key),rules[key]);
						}
					}
					else
					{
						this.addRule(selector,rules,index);
					}
				}
				else
				{
					this.addRule(selector,rules,index);
				}
				return this;
			},
			addRule: function(selector, rules, index)
			{
				if (this.stylesheet.insertRule)
				{
					this.stylesheet.insertRule(selector+"{"+rules+"}",index);
				}
				else
				{
					this.stylesheet.addRule(selector,rules,index);
				}
			},
			getRule: function(index)
			{
				this.getSheet();
				if (this.cssRulesSize>=(index+1))
					return this.stylesheet.cssRules.item(index);
				else
					return false;
			},
			deleteRule: function(index)
			{
				this.getSheet();
				if (this.Parent.isString(index))
					index = this.searchSelector(index);

				if (this.Parent.isInteger(index) && (index != -1))
				{
					if (this.cssRulesSize>=(index+1))
						return this.stylesheet.deleteRule(index);
				}
				else
					return false;
			},
			setRule: function(selector,rules,index)
			{
				return this.addCSSRule(selector,rules,index);
			},
			camelCase: function(value)
			{
				return value.replace(/\-([a-z])/gi, function(match,p1)
				{
					return p1.toUpperCase(); // convert first letter after "-" to uppercase
				});
			},
			// http://www.javascriptkit.com/javatutors/setcss3properties2.shtml
			getcss3prop: function(cssprop)
			{
				var css3vendors = ['', '-moz-','-webkit-','-o-','-ms-','-khtml-'];
				var root = document.documentElement;
				for (var i=0, size=css3vendors.length; i<size; ++i)
				{
					var css3propcamel = this.camelCase(css3vendors[i]+cssprop);
					if (css3propcamel.substr(0,2) == 'Ms') // if property starts with 'Ms'
						css3propcamel = 'm' + this.camelCase.substr(1); // Convert 'M' to lowercase
					if (css3propcamel in root.style)
						return css3propcamel;
				}
				return undefined;
			},


			get: function(element,att)
			{
				att = this.Parent.isNullUndef(att) ? false : att;
				var style = element.getAttribute('style');
					style = (style != null) ? style : '';
				// Return all values from property Style
				if (!att)
				{
					return style;
				} 
				else // Return one specific value from property Style
				{
					var oldstyle = '';
					if (style.indexOf(att+':')!=-1)
					{
						oldstyle = style.substr(style.indexOf(att+':'));
						oldstyle = oldstyle.substr(0,oldstyle.indexOf(';')+1);
						return oldstyle;
					}
					return false;
				}
			},
			add: function(element,att,value)
			{
				if (this.Parent.isObject(att))
				{
					return this.addAsObject(element,att);
				}
				else
				{
					var newstyle = att+':'+value+';';
					var style 	 = this.get(element);
					if (style.indexOf(newstyle)!=-1)
						return true;
					
					var oldstyle = this.get(element,att);
					if (oldstyle)
					{
						newstyle = style.replace(oldstyle,newstyle);
					}
					else
					{
						style += newstyle;
					}
					element.setAttribute('style',newstyle);
					return true;
				}
			},
			addAsObject: function(element,data)
			{
				var newstyle = '';
				var oldstyle = '';
				var style = this.get(element);
				for(var key in data)
				{
					newstyle = key+':'+data[key]+';';
					if (style.indexOf(key) != -1)
					{
						oldstyle = this.get(element,key);
						if (oldstyle)
						{
							style = style.replace(oldstyle,newstyle);
						}
					}
					else
					{
						style += ' '+newstyle;
					}
				}
				element.setAttribute('style',style);
			},
			remove: function(element,att)
			{
				var style 	 = this.get(element);
				var oldstyle = this.get(element,att);
				var newstyle = '';
				if (oldstyle)
				{
					newstyle = style.replace(oldstyle,'');
				}
				if (newstyle !== '')
					element.setAttribute('style',newstyle);
				else
					element.removeAttribute('style');
			}
		};
		this.injectStyle = function(selector,rules)
		{
			this.style.addCSSRule(selector,rules);
			var sClass = selector.replace(".","").replace('#',"");
			if (this.length>0)
			{
				this.foreach(this.eElement,function(object,index,Self){
					Self.class.add(object,sClass);
				});
			}
			else
			if (this.eElement)
			{
				this.class.add(this.eElement,sClass);
			}
		};
		/*
			jx.css("color","#FFF");								// Add Inline Style
			jx.css("color");									// Removes Inline Style
			jx.css({"color":"#FFF"}); 							// Add Inline Style from Object
			jx.css({"color":"#FFF","border":"1px solid #555"}); // Add Inline Style from bigger Object
			jx.css(".cssClass",{"color":"#F00"},true);			// Injects Style
			jx.css(".cssClass",null,true);						// Removes injected Style
		*/
		this.css = function(att,value,inject)
		{
			if (this.eElement)
			{
				if (this.isBoolean(inject))
				{

					if (this.isString(value) || this.isObject(value))
						this.injectStyle(att,value);
					else
						this.injectStyle(att,null);
				}
				else
				if (this.isObject(att))
				{
					this.addStyleAsObject(att);
				}
				else
				if ((!this.isNullUndef(value)) && (value!==''))
				{
					this.addStyle(att,value);
				}
				else
				{
					this.removeStyle(att);
				}
			}
			return this;
		};
		this.hasStyle = function(att)
		{
			var style = this.getAtt('style');
			return ((style != null) && (style.indexOf(att)!=-1))
		};
		this.addStyleAsObject = function(data)
		{
			if (this.length>0)
			{
				this.foreach(this.eElement,function(object,index,Self){
					Self.style.addAsObject(object,data);
				});
			}
			else
			if (this.eElement)
			{
				this.style.addAsObject(this.eElement,data);
			}
			return this;
		};
		this.addStyle = function(att,value)
		{
			if (this.length>0)
			{
				this.foreach(this.eElement,function(object,index,Self){
					Self.style.add(object,att,value);
				});
			}
			else
			if (this.eElement)
			{
				this.style.add(this.eElement,att,value);
			}
			return this;
		};
		this.removeStyle = function(att)
		{
			if (this.length>0)
			{
				this.foreach(this.eElement,function(object,index,Self){
					Self.style.remove(object,att);
				});
			}
			else
			if (this.eElement)
			{
				this.style.remove(this.eElement,att);
			}
			return this;
		};

		/* CLASS OBJECT MANIPULATION */
		this.class =
		{
			Parent: this,
			has: function(element,sClass)
			{
				return element.className.match(new RegExp('(\\s|^)'+sClass+'(\\s|$)')) ? true : false;
			},
			add: function(element,sClass)
			{
				var className = this.Parent.trim(element.className);
				if (this.Parent.isArray(sClass))
				{
					for(var i=0, size=sClass.length; i<size; ++i)
					{
						if (!this.has(element,sClass[i]))
							element.className = className+" "+sClass[i];
					}
				}
				else
				{
					if (!this.has(element,sClass))
						element.className = className+" "+sClass;
				}
			},
			set: function(element,sClass)
			{
				if (this.Parent.isArray(sClass))
				{
					for(var i=0, size=sClass.length; i<size; ++i)
					{
						element.className = this.Parent.trim(sClass[i]);
					}
				}
				else
				{
					element.className = this.Parent.trim(sClass);
				}
			},
			remove: function(element,sClass)
			{
				if (this.Parent.isArray(sClass))
				{
					for(var i=0, size=sClass.length; i<size; ++i)
					{
						this.replace(element,sClass[i],"");
					}
				}
				else
				{
					this.replace(element,sClass,"");
				}
			},
			// http://stackoverflow.com/a/6160317
			// http://stackoverflow.com/a/7487686
			replace: function(element,sClass,sNewClass)
			{
				var sClassName = this.Parent.trim(element.className);
				if (sClassName !== '')
				{
					var reg = new RegExp('(\\s|^)'+sClass+'(\\s|$)');
					element.className = this.Parent.trim(sClassName.replace(reg," "+sNewClass+" "));
				}
			},
			toggle: function(element,sClass,bSwitch)
			{
				bSwitch = (bSwitch!=undefined) ? bSwitch : (this.has(element,sClass));
				if (bSwitch)
				{
					this.remove(element,sClass);
				}
				else
				{
					this.add(element,sClass);
				}
			}
		}
		this.hasClass = function(sClass)
		{
			var Result = [];
			var k = -1;
			if (this.length>0)
			{
				this.foreach(this.eElement,function(object,index,Self){
					Result[++k] = Self.class.has(object,sClass);
				});
				return Result;
			}
			else
			if (this.eElement)
			{
				return this.class.has(this.eElement,sClass);
			}
		};
		this.addClass = function(sClass)
		{
			if (this.length>0)
			{
				this.foreach(this.eElement,function(object,index,Self){
					Self.class.add(object,sClass);
				});
			}
			else
			if (this.eElement)
			{
				this.class.add(this.eElement,sClass);
			}
			return this;
		};
		this.removeClass = function(sClass)
		{
			return this.replaceClass(sClass,"");
		};
		this.replaceClass = function(sClass,sStrToReplace)
		{
			if (this.length>0)
			{
				this.foreach(this.eElement,function(object,index,Self){
					Self.class.replace(object,sClass,sStrToReplace);
				});
			}
			else
			if (this.eElement)
			{
				this.class.replace(this.eElement,sClass,sStrToReplace);
			}
			return this;
		};
		this.toggleClass = function(sClass,bSwitch)
		{
			if (this.length>0)
			{
				var buffer = [];
				var k = -1;
				this.foreachReverse(this.eElement,function(object,index,Self){
					Self.class.toggle(object,sClass,bSwitch);
					// When a "CLASS" is "CHANGED", the Element is "DELETED" from the array.
					// This line, "RESETS" it, so that we can keep using the array.
					buffer[++k] = object;
				});
				this.eElement = buffer;
			}
			else
			if (this.eElement)
			{
				this.class.toggle(this.eElement,sClass,bSwitch);
			}
			return this;
		}
		this.toggleClassPlus = function(sClass1,sClass2,bSwitch)
		{
			if (this.length>0)
			{
				var buffer = [];
				var k = -1;
				this.foreachReverse(this.eElement,function(object,index,Self){
					if (bSwitch)
						this.class.replace(object,sClass1,sClass2);
					else
						this.class.replace(object,sClass2,sClass1);
					// When a "CLASS" is "CHANGED", the Element is "DELETED" from the array.
					// This line, "RESETS" it, so that we can keep using the array.
					buffer[++k] = object;
				});
				this.eElement = buffer;
			}
			else
			if (this.eElement)
			{
				if (bSwitch)
					this.class.replace(this.eElement,sClass1,sClass2);
				else
					this.class.replace(this.eElement,sClass2,sClass1);
			}
			return this;
		}
		/* Hide/Show --- FadeIn/FadeOut */
		this.hide = function(timeout,sClass,hasOpacity,timeoutfade)
		{
			timeout = (this.isInteger(timeout)) ? timeout : false;
			sClass = (this.isString(sClass)) ? sClass : false;
			hasOpacity = (this.isBoolean(hasOpacity)) ? hasOpacity : false;
			imeoutfade = (this.isInteger(timeoutfade)) ? timeoutfade : 60;
			this.hideElement = this.eElement;

			function func(Self)
			{
				Self.set(Self.hideElement); //Faz um reset ao elemento
				if (hasOpacity)
					Self.fadeOut(timeoutfade);
				else
				if (sClass)
					Self.class.add(Self.hideElement,sClass);
				else
					Self.style.add(Self.hideElement,'display','none');
			};
			if (timeout)
			{
				var Self = this;
				setTimeout(function(){func(Self);}, timeout);
			}
			else
				func(this);

			return this;
		};
		this.setShowHide = function(element)
		{
			this.hideElement = element;
		};
		this.show = function(timeout,sClass,hasOpacity,timeoutfade)
		{
			timeout 	= (this.isInteger(timeout)) ? timeout : false;
			sClass 		= (this.isString(sClass)) ? sClass : false;
			hasOpacity 	= (this.isBoolean(hasOpacity)) ? hasOpacity : false;
			timeoutfade = (this.isInteger(timeoutfade)) ? timeoutfade : 60;

			function func(Self)
			{
				Self.set(Self.hideElement); //Faz um reset ao elemento
				if (hasOpacity)
					Self.setOpacity(0.0);
				if (sClass)
					Self.class.remove(Self.hideElement,sClass);
				else
					//Self.css('display','block');
					Self.style.add(Self.hideElement,'display','block');
				if (Self.hasStyle('display'))
					Self.style.add(Self.hideElement,'display','block');
					//Self.css('display','block');

				if (hasOpacity)
					Self.fadeIn(timeoutfade);
			};
			if (timeout)
			{
				var Self = this;
				setTimeout(function(){func(Self);}, timeout);
			}
			else
				func(this);

			return this;
		};
		this.setOpacity = function(range)
		{
			range = this.FormatFloat(range,2);
			this.hideElement.style.filter = "alpha(opacity="+(range*100)+")";
			this.hideElement.style.opacity = range;
			this.hideElement.style.MozOpacity = range;
			this.hideElement.style.KhtmlOpacity = range;
		};
		//http://stackoverflow.com/questions/6121203/how-to-do-fade-in-and-fade-out-with-javascript-and-css
		this.fadeIn = function(timeout)
		{
			timeout = (this.isInteger(timeout)) ? timeout : 40;

			var Self = this;
			var op = 0.0;  // initial opacity
			var timer = setInterval(function()
			{
				op += 0.1;
				Self.setOpacity(op);
				if (op > 1.0)
				{
					Self.css('display','block');
					Self.setOpacity(1.0);
					clearInterval(timer);
				}
			}, timeout);
		};
		this.fadeOut = function(timeout)
		{
			timeout = (this.isInteger(timeout)) ? timeout : 20;

			var Self = this;
			var op = 1.0;  // initial opacity
			var timer = setInterval(function()
			{
				op -= 0.1;
				Self.setOpacity(op);
				if (op < 0.0)
				{
					Self.css('display','none');
					Self.setOpacity(0.0);
					clearInterval(timer);
				}
			}, timeout);
		};

		/* INPUTs/FORMs MANIPULATION */
		this.input =
		{
			Parent: this,
			getobject: function(object)
			{
				return this.Parent.get(object);
			},
			disable: function(object)
			{
				this.getobject(object).setAttribute('disabled','disabled');
				return this;
			},
			enable: function(object)
			{
				this.getobject(object).removeAttribute('disabled');
				return this;
			},
			focus: function(object)
			{
				this.getobject(object).focus();
				return this;
			},
			unfocus: function(object)
			{
				this.getobject(object).blur();
				return this;
			},
			selectIndex: function(object,index)
			{
				this.getobject(object).selectedIndex = index;
				return this;
			},
			setValue: function(object,value)
			{
				this.getobject(object).value = value;
				return this;
			}
		};

		/* VALUES TYPEOF */
		this.isEmpty = function(str)
		{
			return (!str || 0 === str.length);
		};
		this.isBlank = function(str)
		{
			return (!str || /^\s*$/.test(str));
		};
		this.isInteger = function(value)
		{
			return (typeof(value)==='number');
		};
		this.isString = function(value)
		{
			return (typeof(value)==='string'); 
		};
		this.isNullUndef = function(value)
		{
			return ((value == undefined) || (value == null) || (value == false));
		};
		this.isObject = function(value)
		{
			return (typeof(value)==='object'); 
		};
		this.isObjectHasProperty = function(object,property)
		{
			return (this.isObject(object) && (object.hasOwnProperty(property))); 
		};
		this.isNumeric = function(str)
		{
			return (str.match(/^[0-9]+$/)) ? true : false;
		};
		this.isFloat = function(value)
		{
			value = parseFloat(value);
			if (value == 0)
				return true;
			else
				return (value != "") && (!isNaN(value)) && ((Math.round(value) == value) || (Math.round(value) != value));
		};
		this.isDate = function(value)
		{
			return toString.call(value) === '[object Date]';
		};
		this.isBoolean = function(value)
		{
			return (typeof(value) === 'boolean');
		};
		this.isFunction = function(value)
		{
			return (value && (typeof(value) === 'function'));
		};
		this.isArray = function(object)
		{
			//object = (object != undefined) ? object : this.eElement;
			return (object.constructor === Array) ? true : false;
		};

		/* STRING MANIPULATION */
		this.data =
		{
			Parent: this,
			asInteger: function(value)
			{
				return this.Parent.isNumeric(value) ? (value*1) : (value.replace(/\D/g,'')*1);
			},
			asFloat: function(value)
			{
				return this.Parent.isFloat(value) ? (value*1) : (value.replace(/\D/g,'')*1);
			},
			asString: function(value)
			{
				return this.Parent.isString(value) ? value : (value+'');
			},
			IntToStr: function(value)
			{
				return value+'';
			},
			StrToInt: function(value)
			{
				return (value*1);
			},
			trim: function(value)
			{
				//http://blog.stevenlevithan.com/archives/faster-trim-javascript
				return value.replace(/^\s\s*/,'').replace(/\s\s*$/,'');
			},
		};
		this.trim = function(value)
		{
			return this.data.trim(value);
		};
		this.getCurrentTime = function(AsParams)
		{
			var currentTime = new Date();
			var seconds = currentTime.getSeconds();
			var minutes = currentTime.getMinutes();
			var hour 	= currentTime.getHours();
			var day 	= currentTime.getDate();
			var month 	= currentTime.getMonth()+1;
			var year 	= currentTime.getFullYear();
			var weekday = currentTime.getDay();

			if (month<10)
				month 	= '0'+month;
			if (day<10)
				day 	= '0'+day;
			if (hour<10)
				hour 	= '0'+hour;
			if (minutes<10)
				minutes = '0'+minutes;
			if (seconds<10)
				seconds = '0'+seconds;

			AsParams = AsParams.replace(/Y/,year).replace(/m/,month).replace(/d/,day).
								replace(/h/,hour).replace(/i/,minutes).replace(/s/,seconds).
								replace(/w/,seconds);
			return AsParams;
		};
		this.cleanFloatString = function(str)
		{
			str = str.replace(/€/gi,"");
			str = str.replace(",",".");
			str = str.replace(" ","");
			return this.trim(str);
		};
		this.FormatFloat = function(fNum,aCasasDecimais)
		{
			fNum = parseFloat(fNum);
			fNum += 0.00;
			fNum = fNum.toFixed(aCasasDecimais);
			return this.splitNumber(fNum);
		};
		this.splitNumber = function(iNum)
		{
			var parts 	 = iNum.toString().split(".");
				parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
			return parts.join(".");
		};
		this.getRandomString = function(iSize)
		{
			var Result = '';
			var n = 0;
			for(var i=0; i<iSize; ++i)
			{
				n = Math.floor(Math.random()*62);
				if (n<10) Result += n; //1-10
				else if (n<36) Result += String.fromCharCode(n+55); //A-Z
				else Result += String.fromCharCode(n+61); //a-z
			}
			return Result;
		};
		this.IntToStr = function(value)
		{
			return this.data.IntToStr(value);
		};
		this.StrToInt = function (value)
		{
			return this.data.StrToInt(value);
		};
		this.bytesToSize = function(bytes)
		{
			if (bytes == 0)
				return '0 Byte';
			var k = 1000;
			var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
			var i = Math.floor(Math.log(bytes) / Math.log(k));
			return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];
		};
		/* IMAGE ELEMENT MANIPULATION */
		this.image = 
		{
			Parent: this,
			check: function(source,fCallBack)
			{
				var img 	= new Image();
				var imgSrc 	= this.Parent.url.get().host+source;
				var Self 	= this;

				img.onload = function()
					{
						if (Self.Parent.isFunction(fCallBack))
							fCallBack(true,imgSrc);
					};
				img.onerror = function()
					{
						if (Self.Parent.isFunction(fCallBack))
							fCallBack(false,imgSrc);
					};
			
				img.src = imgSrc;
			}
		}
		/* SCRIPTS MANIPULATION */
		this.scripts =
		{
			Parent: this,
			exec: function(param)
			{
				if (this.Parent.isObject(param))
				{
					/* http://stackoverflow.com/a/16278107 */
					var arr = param.getElementsByTagName('script');
					for (var i=0, ilen=arr.length; i<ilen; ++i)
					{
						if (arr[i].getAttribute('exec') == null)
							eval(arr[i].innerHTML);
					}
				}
				else
				if (this.Parent.isString(param))
				{
					eval(param);
				}
			},
		};

		/* ** STRING ENCODING DECODING ** */
		/* 64bits MANIPULATION */
		// https://github.com/kvz/phpjs/blob/master/functions/url/base64_encode.js
		this.base64 =
		{
			Parent: this,
			b64: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',
			encode: function(data)
			{
				var o1, o2, o3, h1, h2, h3, h4, bits = 0,
					i = -1,
					ac = 0,
					enc = '',
					tmp_arr = [];

				if (!data) { return data; }

				data = unescape(encodeURIComponent(data));

				do
				{
					// pack three octets into four hexets
					o1 = data.charCodeAt(++i);
					o2 = data.charCodeAt(++i);
					o3 = data.charCodeAt(++i);

					bits = o1 << 16 | o2 << 8 | o3;
					h1 = bits >> 18 & 0x3f;
					h2 = bits >> 12 & 0x3f;
					h3 = bits >> 6 & 0x3f;
					h4 = bits & 0x3f;

					// use hexets to index into b64, and append result to encoded string
					tmp_arr[++ac] = this.b64.charAt(h1) + this.b64.charAt(h2) + this.b64.charAt(h3) + this.b64.charAt(h4);
				} while (i < data.length);

				enc = tmp_arr.join('');

				var r = data.length % 3;

				return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
			},
			decode: function(data)
			{
				var o1, o2, o3, h1, h2, h3, h4, bits = 0,
					ac = 0,
					i = -1,
					dec = '',
					tmp_arr = [];

				if (!data) { return data; }

				data += '';

				do
				{
					// unpack four hexets into three octets using index points in b64
					h1 = this.b64.indexOf(data.charAt(++i));
					h2 = this.b64.indexOf(data.charAt(++i));
					h3 = this.b64.indexOf(data.charAt(++i));
					h4 = this.b64.indexOf(data.charAt(++i));

					bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;
					o1 = bits >> 16 & 0xff;
					o2 = bits >> 8 & 0xff;
					o3 = bits & 0xff;

					if (h3 == 64) 		{ tmp_arr[++ac] = String.fromCharCode(o1); }
					else if (h4 == 64) 	{ tmp_arr[++ac] = String.fromCharCode(o1, o2); }
					else 				{ tmp_arr[++ac] = String.fromCharCode(o1, o2, o3); }
				} while (i < data.length);

				dec = tmp_arr.join('');

				return decodeURIComponent(escape(dec.replace(/\0+$/, '')));
			},
		};
		this.encode64 = function(sString)
		{
			return this.base64.encode(sString);
		};
		this.decode64 = function(sString)
		{
			return this.base64.decode(sString);
		};
		/* JSON MANIPULATION */
		this.ParseJSON = function(AString,ADecode)
		{
			AString = ((ADecode != undefined) && (ADecode == true)) ? this.decode64(AString) : AString;
			try
			{
				return JSON.parse(AString);
			}
			catch(e)
			{
				console.log(e);
				console.log(AString);
				return false;
			}
		};

		/* Ajax/Params/Urls MANIPULATION */
		this.url =
		{
			Parent: this,
			set: function(value)
			{
				document.location.href = value;
			},
			get: function(sURL)
			{
				sURL = this.getLocation(sURL);
				var data = {};
				data.host = this.getHost(sURL)+'/';
				data.scheme = this.getScheme(data.host);
				data.path = {};
				data.url = sURL;
				
				// break at .com,.pt,.uk AND gets content after "/"
				var param = sURL.substr(sURL.indexOf('.')+1); 
					param = param.substr(param.indexOf('/')+1);

				var fragment = param.indexOf('#');
				if (fragment != -1)
				{
					fragment = param.substr(fragment+1);
					param = param.substr(1,fragment-1);
					data.frament = fragment;
				}
				data.params = param;

				var arrParams = param.split('/');
				for(var i=0, ilen=arrParams.length; i<ilen; i+=2)
				{
					if (arrParams[i] !== '')
						data.path[arrParams[i]] = arrParams[i+1];
				}

				return data;
			},
			getLocation: function(sURL)
			{
				return ((!this.Parent.isNullUndef(sURL)) && (sURL !== '')) ? sURL : window.location.href;
			},
			getHost: function(sURL)
			{
				sURL = this.getLocation(sURL);
				sURL = sURL.replace('://','#');
				return sURL.substr(0,sURL.indexOf('/')).replace('#','://');
			},
			getScheme: function(sURL)
			{
				sURL = this.getLocation(sURL);
				return sURL.substr(0,sURL.indexOf('://')+3);
			},
			getParam: function(AsParam,sURL)
			{
				sURL = this.getLocation(sURL);
				var regex 	= new RegExp("[\\?&]"+AsParam+"=([^&#]*)");
				var results = regex.exec(sURL);
				return (!this.Parent.isNullUndef(results)) ? '' : results[1];
			},
			setParam: function(sURL,sName,sNewValue,bRetriveHOST)
			{
				sURL = this.getLocation(sURL);
				if (sURL.indexOf('?') != -1)
				{
					var arrAux 	= sURL.split('?');
					var ResVal 	= this.getParam(sName,arrAux[1]);
						Result 	= arrAux[1].replace(sName+'='+ResVal,sName+'='+sNewValue);
					var sHOST   = (bRetriveHOST) ? arrAux[0] : '';
					return sHOST+'?'+Result;
				}
				return sURL;
			},
			redirect: function(sURL)
			{
				window.location.assign(this.get().host+sURL);
			},
		};

		this.params =
		{
			Parent: this,
			param: '',
			get: function()
			{
				return this.param;
			},
			set: function(sValue)
			{
				this.param = sValue;
				return this;
			},
			add: function(sOption,sValue,bReset)
			{
				if (bReset==true)
					this.reset();

				this.param += ((this.param==='')?'':'&')+sOption+'='+sValue;
				return this;
			},
			reset: function()
			{
				this.param = '';
				return this;
			},
		};

		/* AJAX MANIPULATION*/
		this.newXMLHTTP = function()
		{
			var xmlhttp = false;
			if (window.XMLHttpRequest)
			{// code for IE7+, Firefox, Chrome, Opera, Safari
				return new XMLHttpRequest();
			}
			else
			{// code for IE6, IE5
				var sxmlhttp = ['Microsoft.XMLHTTP','Msxml2.XMLHTTP.5.0',
								'Msxml2.XMLHTTP.4.0','Msxml2.XMLHTTP.3.0','Msxml2.XMLHTTP'];
				for (var i=0; i<5; ++i)
				{
					try
					{	//Internet Explorer
						xmlhttp = new ActiveXObject(sxmlhttp[i]);
						break;
					}
					catch (e)
					{
						console.log('[ERROR] XMLHTTPRequest could not be created! ('+sxmlhttp[i]+')');
						xmlhttp = false;
					}
				}
			}
			return xmlhttp;
		};
		this.ajax = function(AoData)
		{
			if (!this.isNullUndef(AoData))
			{	
				var Parent = this;
				var XMLHTTP = this.newXMLHTTP();
				if (XMLHTTP != false)
				{
					AoData.url += (AoData.url.indexOf('?') == -1) ? '?' : '';
					if (this.isObject(AoData.data))
					{
						AoData.data.map(function(elem){ return elem.name; }).join("&");
					}

					if (!AoData.hasOwnProperty('async'))
						AoData.async = true;
					
					XMLHTTP.onreadystatechange = function()
					{
						if (this.readyState == 4)
						{
							if (this.status==200)
							{
								if (Parent.isFunction(AoData.done))
									AoData.done(Parent.trim(this.responseText));
							}
							else
							{
								console.log("[Ajax]Error: returned status code "+this.status+" - "+this.statusText);
								console.log("[Ajax]Error: "+this.responseText);
							}
						}
					/*
						switch(this.readyState)
						{
							case 0: { } break; // UNSENT -- open() has not been called yet.
							case 1: { } break; // OPENED -- send() has not been called yet.
							case 2: {
										// HEADERS_RECEIVED -- send() has been called. Headers and Status are available.
										//	console.log(this.getAllResponseHeaders()); 
									} break;
							case 3: { } break; // LOADING -- Downloading; responseText holds partial data.
							case 4:
								{	// DONE -- The operation is complete.
									if (this.status==200)
									{
										//var fCallback = AoData.done;
										if (Parent.isFunction(AoData.done))
											AoData.done(Parent.trim(this.responseText));
									}
								} break;
						}
					*/
					};

					switch(AoData.type)
					{
						case 'GET' :
							{
								
								XMLHTTP.open('GET',AoData.url+AoData.data,AoData.async);
								XMLHTTP.send(null);
							} break;
						case 'POST':
							{
								XMLHTTP.open("POST",AoData.url,AoData.async);
								XMLHTTP.setRequestHeader("Content-type","application/x-www-form-urlencoded");
								XMLHTTP.send(AoData.data);
							} break;
					}
				}
				return XMLHTTP;
			}
			return this;
		};
		this.extend = function(object)
		{
			this[object.name] = object;
			if (object.hasOwnProperty('Parent'))
				object.Parent = this;
		}
	};
	var template =
	{
		name: 'template',
		Parent: jx,
		html: '',
		eTarget: false,
		setTemplate: function(html)
		{
			this.html = html;
		},
		setTarget: function(object)
		{
			this.eTarget = this.Parent.get(object);
		},
		render: function(objects,fCallback,fCallbackAfter)
		{
			var Result = '';
			var auxRes = '';
			//var isFunc  = this.Parent.isFunction(fCallback);
			//var isFunc2 = this.Parent.isFunction(fCallbackAfter);
			for (var i=0, ilen=objects.length; i<ilen; ++i)
			{
				//if (isFunc) { objects[i] = fCallback(objects[i],i); }
				auxRes = this.renderOne(objects[i]);
				//if (isFunc2) { fCallbackAfter(objects[i],i,Result); }
				Result += auxRes;
			};
			return Result;
		},
		renderOne: function(objData)
		{
			var Result = this.html;
			for(var key in objData)
			{
				Result = Result.replace(new RegExp('{'+key+'}','g'), objData[key]);
			}
			return Result;
		},
		append: function(html,elemTarget)
		{
			this.Parent.set(elemTarget).replaceWith(html);
		},
	};

	var message = 
	{
		name:'message',
		Parent: jx,
		eDivAlert: false, 	// id=eDivAlert
		eBlackout: false, 	// id=eBlackout
		eTitle: false, 		// id=eTitleAlert
		eMessage: false, 	// id=eMessageAlert
		tTimer: false,
		left: 0,
		top: 0,

		classBlackout: '',
		classDivAlert: '',
		classTitle: '',
		classMessage: '',
		setLeft: function(value)
		{
			this.left = value;
		},
		setTop: function(value)
		{
			this.top = value;
		},
		setStyles: function()
		{
			if (this.classBlackout === '')
			{
				this.Parent.set(this.eBlackout).css({'display':'block',
													  'position':'fixed',
													  'top':'0px',
													  'left':'0px',
													  'width':'100%',
													  'height':'100%',
													  'background':'rgba(0,0,0,0.55)',
													  'text-align':'center',
													  'vertical-align':'middle',
													  'z-index':'9998',
													  'opacity':'0.0',
													});
			}
			else
				this.Parent.set(this.eBlackout).addClass(this.classBlackout);

			if (this.classDivAlert === '')
			{
				this.Parent.getPageScrollPos();
				if (this.Parent.SCROLL_POSY>=0)
					this.top = (this.Parent.SCROLL_POSY)+'px';

				this.top = (this.top == 0) ? this.top+'px' : this.top;
				this.Parent.set(this.eDivAlert).css({'display':'inline-block',
													 'vertical-align':'middle',
													 //'width':'40%',
													 'margin-top':'calc('+this.top+' + 64px)',
													 //'margin-left':'calc('+this.left+' + '+(window.innerWidth/2)+'px)',
													 //'padding':'0px 1em 2em 1em',
													 'background':'rgba(255,255,255,1)',
													 'border-radius':'4px',
													 'border':'1px solid #525151',
													 //'-webkit-box-shadow':'1px 1px 12px rgba(0, 0, 0, 0.25)',
													 'box-shadow':'1px 1px 12px rgba(0, 0, 0, 0.6)',
													 //'box-shadow':'inset 0px 0px 25px -3px rgba(0, 0, 0, 0.3)',
													 'position':'absolute',
													 'z-index':'9999',
													});
			}
			else
				this.Parent.set(this.eDivAlert).addClass(this.classDivAlert);

			this.Parent.set(this.eTitle).css({'font-weight':'bold',
												'font-size':'1.1em',
												'padding':'0.4em',
												'margin-top':'0px',
												'text-align':'center',
												'background':'#3f4b5c',
												'color':'#fafafa',
												'overflow':'hidden',
												'border-radius':'3px 3px 0px 0px',
												});
			this.Parent.set(this.eMessage).css({'padding':'0.8em',
												'display':'inline-block',
												});
			var Self = this;
			setTimeout(function(){
				Self.left = (Self.left == 0) ? Self.left+'px' : Self.left;
				Self.Parent.set(Self.eDivAlert).css({'margin-left':'calc('+Self.left+' + 50% - '+(Self.eDivAlert.offsetWidth/2)+'px)'});
			},1);
		},
		setClasses: function(Blackout,DivAlert,Title,Message)
		{
			this.classBlackout  = this.Parent.isString(Blackout) ? Blackout : '';
			this.classDivAlert 	= this.Parent.isString(DivAlert) ? DivAlert : '';
			this.classTitle 	= this.Parent.isString(Title) 	 ? Title : '';
			this.classMessage 	= this.Parent.isString(Message)  ? Message : '';
		},
		getElements: function()
		{
			this.eDivAlert 	= this.Parent.get('#eDivAlert',0);
			this.eBlackout  = this.Parent.get('#eBlackout',0);
			this.eTitle 	= this.Parent.get('#eTitleAlert',0);
			this.eMessage 	= this.Parent.get('#eMessageAlert',0);
			if (this.Parent.isNullUndef(this.eDivAlert))
			{
				var elements = '';
					elements += '<div id="eDivAlert">';
					elements += 	'<div id="eTitleAlert"></div>';
					elements += 	'<span id="eMessageAlert"></span>';
					elements += '</div>';
				 	elements += '<div id="eBlackout" style="display:none;"></div>';

				document.body.insertAdjacentHTML('afterbegin',elements);
				this.eDivAlert  = this.Parent.get('#eDivAlert',0);
				this.eBlackout  = this.Parent.get('#eBlackout',0);
				this.eTitle 	= this.Parent.get('#eTitleAlert',0);
				this.eMessage 	= this.Parent.get('#eMessageAlert',0);
				var Self = this;
				this.Parent.set(this.eBlackout).addEventHandler('click',function(event){
					Self.hide();
					clearTimeout(Self.tTimer);
				});
			}
			this.Parent.setShowHide(this.eBlackout);
		},
		hide: function()
		{
			this.Parent.set(this.eDivAlert).addClass('hidden');
			this.Parent.set(this.eBlackout).hide(1,false,true,30);
		},
		show: function(sTitle,sMessage,Timeout,Redirect)
		{
			if (!this.Parent.isInteger(Timeout))
			{
				if (Timeout == undefined)
					Timeout = 3000;
			}

			Redirect = (Redirect != undefined) ? Redirect : false;
			//sMessage = sMessage.replace("\n",'<br/>');
			
			this.getElements();

			this.Parent.set(this.eTitle);
			if (this.Parent.isObject(sTitle))
			{
				this.Parent.html(sTitle.title);
				switch(sTitle.type)
				{
					case 'sucess': 	this.Parent.css({'background-color':'#9ed44e'}); break;
					case 'error': 	this.Parent.css({'background-color':'#f05959'}); break;
					case 'info': 	this.Parent.css({'background-color':'#59b6f0'}); break;
					case 'normal': 	this.Parent.css({'background-color':'#3f4b5c'}); break;
				}
			}
			else
			if (sTitle !== '')
				this.Parent.html(sTitle);
			else
			if ((!this.Parent.isString(this.classTitle)) && (this.classTitle!==''))
				this.Parent.addStyle('margin-top','-0.8em');

			this.Parent.set(this.eMessage).html(sMessage);
			this.setStyles();
			this.Parent.set(this.eDivAlert).removeClass('hidden');
			this.Parent.set(this.eBlackout).show(1,false,true,15);

			var Self = this;
			if (Timeout) // NAO ESCONDE se eu não quiser!
			{
				this.tTimer = setTimeout(function()
				{
					if (Redirect != false)
					{
						Self.Parent.url.redirect(Redirect);
					}
					else
					{
						Self.hide();
					}
				}, Timeout);
			}
		},
	}
	var effects =
	{
		name: 'effects',
		slide: function(movement)
		{
			console.log('effects.slide("'+movement+'")');
		},
	};

	var jx = new elem();
		jx.extend(effects);
		jx.extend(template);
		jx.extend(message);
