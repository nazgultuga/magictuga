	
	/* ************************************** */
	/* ************************************** */

	function getShadowWindow()
	{
		return jx.get('#win-shadow');
	}
	function jxOpenWindow(AsWindowName)
	{
		var url = '/tpl-php/'+AsWindowName+'.php';
		var params = '';

		var fCallback=function(sResult)
		{
			jx.set('#win-shadow').removeClass('hidden').html(sResult);
		}

		jx.ajax({type:'POST', url:url, data:params, done:fCallback});
	}
	function jxCloseWindow()
	{
		jx.set('#win-shadow').addClass('hidden');
	}

	function jxToggleForm(AsFormName,AsOperation)
	{
		jx.forms.submit(AsFormName,AsOperation,false);
	}

	function getIndex_ElementByDataID(AHTML,aTagName,aDataName,aName)
	{
		var arrObjs = AHTML.children;
		for (var i=0, ilen=arrObjs.length; i<ilen; ++i)
		{
			if (arrObjs[i].getAttribute(aDataName) === aName)
			{
				return i;
			}
		}
		return -1;
	}

/* ******************************************************************** */
/* ******************************************************************** */
	function actualizarCartaStock(Self)
	{
		var multiverseid = Self.getAttribute('data-multi');

		var params  = '&multiverseid='+multiverseid;
			params += '&ajax=true';
			params += '&stock='+Self.selectedIndex;

		jx.ajax({type:'POST', url:'/magic/core/db_toggle/ajax-update_cardstock.php?', data:params,
			done:function(sResult)
			{
				console.log(sResult);
			}
		});
	};
/* ******************************************************************** */
	function actualizarPrecos(Self,listobjects,index)
	{

		if (Self != undefined)
		{
			var eTable 	= Self.parentNode.children[1];
			var eTBody 	= eTable.children[1];
			var listobjects= {};
			for(var i=0, ilen=eTBody.children.length; i<ilen; ++i)
			{
				listobjects[i] = {
					multiverseid: eTBody.children[i].children[0].innerHTML,
					name: 		eTBody.children[i].children[1].innerHTML.replace(/ /g,'+'),
					collection: eTBody.children[i].children[2].innerHTML,
					eTDPreco: 	eTBody.children[i].children[3],
				}
			}
			listobjects.length = eTBody.children.length;
		}
		index = (index !== undefined) ? index : 0;

		var ipos1 = 0;
		var ipos2 = 0;
		var params = '&card_name='+listobjects[index]['name'];
		jx.ajax({type:'POST', url:'/magic/core/db_toggle/ajax-getmagictuga.php?', data:params,
			done:function(sResult)
			{
				//console.log(sResult);
				if (sResult !== 'false')
				{
					var iPos = sResult.indexOf(listobjects[index]['collection']);
					if (iPos != -1)
					{
						sResult = sResult.substr(iPos);
						iPos = sResult.indexOf('</td><td ><input type=hidden');
						if (iPos == -1)
							iPos = sResult.indexOf('</td><td ><a href="esgotado');
						sResult = sResult.substr(0,iPos);

						iPos = sResult.lastIndexOf('</td><td>');
						sResult = sResult.substr(iPos+9);
						if (sResult === '')
							sResult = '0.0';
					}
					else
					{
						sResult = '0.0';
					}
					
					listobjects[index].price = sResult;
					listobjects[index].eTDPreco.innerHTML = sResult;
				}
				if (index<(listobjects.length-1))
					actualizarPrecos(undefined,listobjects,index+1);
				else
				{
					var data = [];
					for(var i=0, ilen=listobjects.length; i<ilen; ++i)
					{
						data[i] = "UPDATE `magictuga`.`cards` SET `price` = '"+listobjects[i].price+"' WHERE `cards`.`multiverseid` = "+listobjects[i].multiverseid+";";
					}
					console.log(data.join("\n"));
				}
			}
		});
	};
/* ******************************************************************** */
/* ******************************************************************** */
/* ******************************************************************** */

	function parseMagicInformation(Self)
	{
		var html = Self.parentNode.children[0].value;
			html = jx.ParseHTMLToDOM(html);
			html = html.children[0]; // BODY
			html = html.children[0]; // TR
		var auxiliar = '';

		var ImageInfo 	= html.children[0]; // TD
		var Details 	= html.children[1]; // TD
		var Others 		= html.children[2].children[0]; // TD/SMALL

		var edtNameEng 	= jx.get('name_eng');
		var edtNamePT 	= jx.get('name_pt');
		var edtNumber 	= jx.get('number');
		var edtManaCost = jx.get('mana_cost');
		var edtPoints 	= jx.get('points');
		var selRare 	= jx.get('rare');
		var edtDescriptionEng 	= jx.get('description_eng');
		var edtDescriptionPT 	= jx.get('description_pt');
		var edtImgURL 	= jx.get('image_url');


			edtImgURL.value 	= ImageInfo.children[3].src;
		var auxiliar = getIndex_ElementByDataID(Others,'IMG','alt','English');
			edtNameEng.value 	= Others.children[auxiliar+1].innerHTML;
			edtNamePT.value 	= Details.children[0].children[0].innerHTML;
		var number = Others.children[2].innerHTML.split(" (");
			edtNumber.value 	= number[0].replace("#",'');
		var textType 			= Details.children[1].innerHTML.split(", ");
		var auxiliar 			= textType[0].substr(textType[0].lastIndexOf(' ')+1);
			edtPoints.value 	= (auxiliar.indexOf('/') != -1) ? auxiliar : '';
			edtManaCost.value	= jx.trim(textType[1].substr(1,textType[1].indexOf('(')-1));
			
			edtDescriptionEng.value = Details.children[3].children[2].children[0].innerHTML;
			edtDescriptionPT.value = Details.children[2].children[0].innerHTML;
	}

	function repositionPages()
	{
		var pages = jx.get('.pagination',0);
		var father= jx.get({tag:'DIV',att:'data-id',name:'search-results'},0);
		if (father.children.length>=3)
			father.children[0].insertAdjacentHTML('afterend',pages.outerHTML);
		else
			father.children[0].insertAdjacentHTML('beforebegin',pages.outerHTML);
	};
	function searchCards(Self)
	{
		var params  = '&ajax=true';
			params += '&search='+Self.value;

		jx.ajax({type:'GET', url:'/magic/core/db_toggle/ajax-cardsSearch.php?', data:params,
			done:function(sResult)
			{
				jx.set({tag:'DIV',att:'data-id',name:'search-results'},0).html(sResult);
			}
		});
	};
	function openCardInfo(Self)
	{
		var jsondata 	 = Self.parentNode.parentNode.children[2].value;
			jsondata 	 = jx.ParseJSON(jsondata,true);
		var multiverseid = jsondata.multiverseid;
		var collection   = jsondata.collection_eng;
		var collectSet   = jsondata.magic_set;
		var rarity 		 = jsondata.rarity;

		var title  = '<div class="collect-title">';
			title += '<img src="http://gatherer.wizards.com/Handlers/Image.ashx?type=symbol&set='+collectSet+'&size=large&rarity='+rarity+'">';
			title += collection;
			title += '</div>';
	

		var params  = '&multiverseid='+multiverseid;
			params += '&ajax=true';
		jx.ajax({type:'GET', url:'/magic/core/db_toggle/ajax-cardinfo.php?', data:params,
			done:function(sResult)
			{
				jx.message.show(title,sResult,false);
			}
		});
	};

	function addCard(Self)
	{
		var eParent 	 = Self.parentNode.parentNode;
		var multiverseid = eParent.children[1].value;
		var jsondata 	 = eParent.children[2].value;
			jsondata 	 = jx.ParseJSON(jsondata,true);
			jsondata.qtd = eParent.children[13].children[0].value;

		var params  = '&multiverseid='+multiverseid;
			params += '&ajax=true';
			params += '&action=toggle';
			params += '&data='+JSON.stringify(jsondata);
		jx.ajax({type:'POST', url:'/magic/core/db_toggle/shoppingcart.php?', data:params,
			done:function(sResult)
			{
				jsonData = jx.ParseJSON(sResult);
				if (jsonData.result)
				{
					jx.set('#menu-shopcart-cards',0).html(jsonData.total_cartas);
					jx.set('#menu-shopcart-total',0).html(jsonData.total_pagar);
				}
				else
				{

				}
			}
		});
	};
	function updateShopTotais()
	{
		var etBody = jx.get('*TBODY',0);

		var totalall 	= 0.0;
		var numCartas	= 0;
		var tMagic 		= 0.0;
		var tMagicAll 	= 0.0;
		var eSelect 	= false;
		console.log(etBody.children);
		for(var i=0, ilen=etBody.children.length-1; i<ilen; ++i)
		{
			eSelect 	= jx.get(etBody.children[i].children[5].children[0]);
			eSelect 	= (eSelect.tagName === 'OPTION') ? eSelect.parentNode : eSelect;
			numCartas	+= eSelect.selectedIndex+1;
			totalall 	+= 1*jx.set(etBody.children[i].children[9]).html().replace('€','');
			tMagicAll 	+= 1*jx.set(etBody.children[i].children[8]).html().replace('€','');
		}
		
		var eNumCards = etBody.children[etBody.children.length-1].children[2];
		if (numCartas != 0)
			jx.set(eNumCards).html(numCartas);
		else
			jx.get(eNumCards).innerHTML = numCartas;
		jx.set(etBody.children[etBody.children.length-1].children[5]).html(jx.FormatFloat(tMagicAll,2)+'€');
		jx.set(etBody.children[etBody.children.length-1].children[6]).html(jx.FormatFloat(totalall,2)+'€');
		var totalfim = jx.get('.cart-shipp2',1);
			jx.set(totalfim.children[6]).html(jx.FormatFloat(totalall+3.90,2)+'€');
	}
	function updateQuantity(Self)
	{
		var eParent 	 = Self.parentNode.parentNode;
		var etBody 		 = eParent.parentNode;
		var multiverseid = eParent.children[1].value;
		var objJson 	 = jx.ParseJSON(eParent.children[2].value,true);
			objJson.qtd  = Self.selectedIndex+1;
		var novo_total 	 = jx.FormatFloat(objJson.qtd*objJson.price,2);

		var params  = '&multiverseid='+multiverseid;
			params += '&ajax=true';
			params += '&action=toggle';
			params += '&data='+JSON.stringify(objJson);
		jx.ajax({type:'POST', url:'/magic/core/db_toggle/shoppingcart.php?', data:params,
			done:function(sResult)
			{
				console.log(sResult);
				jsonData = jx.ParseJSON(sResult);
				if (jsonData.result)
				{
					var tNormal = 1*jx.set(eParent.children[6]).html().replace('€','');
					var tMagic  = 1*jx.set(eParent.children[7]).html().replace('€','');
						tNormal = jx.FormatFloat(objJson.qtd*tNormal,2);
						tMagic  = jx.FormatFloat(objJson.qtd*tMagic,2);

					jx.set(eParent.children[8]).html(tMagic+'€');
					jx.set(eParent.children[9]).html(tNormal+'€');
					jx.get('#menu-shopcart-cards',0).innerHTML = jsonData.total_cartas;
					jx.get('#menu-shopcart-total',0).innerHTML = jsonData.total_pagar;

					updateShopTotais();
				}
				else
				{
					jx.message.show('Erro ao gravar','Ocorreu um erro ao alterar Quantidade, por favor tente de novo.',6000);
				}
			}
		});
	}
	function removeCard(Self)
	{
		var multiverseid = Self.parentNode.children[1].value;
		var objJson = {};

		var params  = '&multiverseid='+multiverseid;
			params += '&ajax=true';
			params += '&action=remove';
			params += '&data='+JSON.stringify(objJson);
		jx.ajax({type:'POST', url:'/magic/core/db_toggle/shoppingcart.php?', data:params,
			done:function(sResult)
			{
				console.log(sResult);
				jsonData = jx.ParseJSON(sResult);
				if (jsonData.result)
				{
					jx.set(Self.parentNode).remove();
					jx.get('#menu-shopcart-cards',0).innerHTML = jsonData.total_cartas;
					jx.get('#menu-shopcart-total',0).innerHTML = jsonData.total_pagar;
					updateShopTotais();
					//jx.message.show('','Carta removida com sucesso!',1400);
				}
				else
				{

				}
			}
		});
	};





